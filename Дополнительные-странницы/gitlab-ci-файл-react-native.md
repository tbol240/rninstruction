```
# этапы pipline, разделяет логически работы на этапы, наприер для разных платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый раннер
  # Этап сборки проекта при использовании раннера
  - Build

# Глобальное клюевое слово, позволяющее установить глобальное значение для других ключевых слов (https://docs.gitlab.com/ee/ci/yaml/#default)
default:
  # Используйте before_script, чтобы определить массив команд, которые должны выполняться перед командами сценария каждого задания, но после восстановления артефактов.(https://docs.gitlab.com/ee/ci/yaml/#before_script)
  before_script:
    - node -v
    # установка необходимых для сборки пакетов
    - yarn install

# job для сборки проекта для дебага на Android
Android-Debug-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - windows
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    - cd android
    - ./gradlew assembleDebug
    - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-debug-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/apk/debug/app-debug.apk

# job для сборки проекта для дебага на Android
Android-Debug-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build

  variables:
    # шлях скачування секретних файлів добавлених через gitlab. Дефолтне значення - root проекту
    # SECURE_FILES_DOWNLOAD_PATH: "./"
    #  MYAPP_UPLOAD_STORE_FILE: "./my-upload-key.keystore"
    #  MYAPP_UPLOAD_STORE_PASSWORD: "password"
    #  MYAPP_UPLOAD_KEY_ALIAS: "my-upload-key"
    #  MYAPP_UPLOAD_KEY_PASSWORD: "password"
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    - cd android
    - chmod +x gradlew
    - ./gradlew assembleDebug
    - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-debug-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/apk/debug/app-debug.apk

# job для android реліз сборки
Android-Release-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # змінні для цього job
  variables:
    # шлях скачування секретних файлів добавлених через gitlab. Дефолтне значення - root проекту
    SECURE_FILES_DOWNLOAD_PATH: "./"
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - windows
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # змінюємо директорію
    - cd android/app
    # скачуємо .keystore файл добавлений через gitlab
    - powershell -Command "& { (Invoke-WebRequest -Uri https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer.ps1 -UseBasicParsing).Content | Invoke-Expression }"
    # змінюємо директорію android
    - cd ../
    # запускаємо реліз сборку проекту
    - ./gradlew :app:bundleRelease
    # зупиняємо ./gradlew після завершення - інколи цей процес сам не зупиняється
    - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/bundle/release/app-release.aab

# job для android реліз сборки
Android-Release-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # змінні для цього job
  variables:
    # шлях скачування секретних файлів добавлених через gitlab. Дефолтне значення - root проекту
    SECURE_FILES_DOWNLOAD_PATH: "./"
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    - cd android
    - chmod +x gradlew
    # змінюємо директорію
    # - cd app
    # скачуємо .keystore файл добавлений через gitlab
    # - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
    # змінюємо директорію android
    # - cd ../
    # запускаємо реліз сборку проекту
    - ./gradlew :app:bundleRelease
    # зупиняємо ./gradlew після завершення - інколи цей процес сам не зупиняється
    # - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/bundle/release/app-release.aab

Android-Auto-Release-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # змінні для цього job
  variables:
    # шлях скачування секретних файлів добавлених через gitlab. Дефолтне значення - root проекту
    # SECURE_FILES_DOWNLOAD_PATH: "./"
     MYAPP_UPLOAD_STORE_FILE: "./my-upload-key.keystore"
     MYAPP_UPLOAD_STORE_PASSWORD: "password"
     MYAPP_UPLOAD_KEY_ALIAS: "my-upload-key"
     MYAPP_UPLOAD_KEY_PASSWORD: "password"
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    - echo $MYAPP_UPLOAD_STORE_FILE
    - cd android
    - chmod +x gradlew
    # змінюємо директорію
    - cd app
    # скачуємо .keystore файл добавлений через gitlab
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
    # змінюємо директорію android
    - cd ../
    # запускаємо реліз сборку проекту
    - ./gradlew :app:bundleRelease
    # зупиняємо ./gradlew після завершення - інколи цей процес сам не зупиняється
    - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/bundle/release/app-release.aab

Android-Auto-Debug-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    - cd android
    - chmod +x gradlew
    - ./gradlew assembleDebug
    - ./gradlew --stop
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: android-debug-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - android/app/build/outputs/apk/debug/app-debug.apk


IOS-Debug-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # Виводимо ім'я користувача
    - whoami
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо дебаг білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Dnevniksnamladencatreker.xcworkspace -scheme Dnevniksnamladencatreker -destination 'generic/platform=iOS Simulator' -configuration Debug
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/Dnevniksnamladencatreker -workspace Dnevniksnamladencatreker.xcworkspace -scheme Dnevniksnamladencatreker -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug
  artifacts:
    name: ios-debug-app
    expire_in: 2 hrs
    when: on_success
    paths:
      - ios/build/Dnevniksnamladencatreker.xcarchive

# Job для сборки по кліку ios реліз білду
IOS-Release-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # Виводимо ім'я користувача
    - whoami
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо реліз білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS Simulator' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/Knots3D -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: ios-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ios/build/Knots3D.xcarchive

IOS-Deploy:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Параметр job, указывающий на условия начала работы, может быть при указаниии тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # Виводимо ім'я користувача
    - whoami
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    - touch ExportOptions.plist
    - echo '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><dict><key>method</key><string>app-store</string><key>teamID</key><string>Y6TZPN9V5V</string></dict></plist>' >> ExportOptions.plist
    # встановлюємо залежності в ios
    - pod install
    # робимо реліз білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/Knots3D -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
    - xcodebuild -exportArchive -quiet -archivePath ./build/Knots3D.xcarchive -exportPath ./ -exportOptionsPlist 'ExportOptions.plist' -configuration Release
    - xcrun altool --validate-app -f ./Knots3D.ipa -t ios -u $IOS_USERNAME -p $IOS_APP_SPECIFIC_PASS
    - xcrun altool --upload-app -f ./Knots3D.ipa -t ios -u $IOS_USERNAME -p $IOS_APP_SPECIFIC_PASS CODE_SIGN_STYLE=Automatic
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: ios-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ios/build/Knots3D.xcarchive      

# Job для сборки по кліку ios реліз білду
IOS-Auto-Release-Build:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленых в stages
  stage: Build
  # Теги для указание какой раннер может брать эту работу. Используются для избижания ситуаций, когда например Windows пытается собрать iOS билд без xCode
  tags:
    - macOS
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # Виводимо ім'я користувача
    - whoami
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо реліз білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS Simulator' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/Knots3D -workspace Knots3D.xcworkspace -scheme Knots3D -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
  # artifacts - файлы созданые job в процессе работы, они выгружаються по указаному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: ios-release-app
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ios/build/Knots3D.xcarchive



```