```
source 'https://github.com/cleveradssolutions/CAS-Specs.git'\n
source 'https://cdn.cocoapods.org/'\n
# Uncomment the next line to define a global platform for your project\n
platform :ios, '12.0'\n
\n
target 'Unity-iPhone' do\n
  # Comment the next line if you don't want to use dynamic frameworks\n
  use_frameworks!\n
\n
  # Pods for Unity-iPhone\n
\n
  target 'Unity-iPhone Tests' do\n
    inherit! :search_paths\n
    # Pods for testing\n
  end\n
\n
end\n
\n
target 'UnityFramework' do\n
  # Comment the next line if you don't want to use dynamic frameworks\n
  use_frameworks!\n
\n
  # Pods for UnityFramework\n
 pod 'CleverAdsSolutions-Base', '3.2.1'\n
\n
end\n
post_install do |installer|\n
  installer.pods_project.targets.each do |target|\n
      target.build_configurations.each do |config|\n
        xcconfig_path = config.base_configuration_reference.real_path\n
        xcconfig = File.read(xcconfig_path)\n
        xcconfig_mod = xcconfig.gsub(/DT_TOOLCHAIN_DIR/, "TOOLCHAIN_DIR")\n
        File.open(xcconfig_path, "w") { |file| file << xcconfig_mod }\n
      end\n
  end\n
end
```

# PODfile для GoogleMobileAds

```
source 'https://github.com/CocoaPods/Specs'\n
source 'https://cdn.cocoapods.org/'\n
# Uncomment the next line to define a global platform for your project\n
platform :ios, '12.0'\n
\n
target 'Unity-iPhone' do\n
  # Comment the next line if you don't want to use dynamic frameworks\n
  use_frameworks!\n
\n
  # Pods for Unity-iPhone\n
\n
  target 'Unity-iPhone Tests' do\n
    inherit! :search_paths\n
    # Pods for testing\n
  end\n
\n
end\n
\n
target 'UnityFramework' do\n
  # Comment the next line if you don't want to use dynamic frameworks\n
  use_frameworks!\n
\n
  # Pods for UnityFramework\n
 pod 'Google-Mobile-Ads-SDK', '~> 10.9'\n
\n
end\n
post_install do |installer|\n
  installer.pods_project.targets.each do |target|\n
      target.build_configurations.each do |config|\n
        xcconfig_path = config.base_configuration_reference.real_path\n
        xcconfig = File.read(xcconfig_path)\n
        xcconfig_mod = xcconfig.gsub(/DT_TOOLCHAIN_DIR/, "TOOLCHAIN_DIR")\n
        File.open(xcconfig_path, "w") { |file| file << xcconfig_mod }\n
      end\n
  end\n
end
```