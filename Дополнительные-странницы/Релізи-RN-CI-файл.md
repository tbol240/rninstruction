```
# этапы pipline, разделяет логически работы на этапы, наприер для разных платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый раннер
  # Этап сборки проекта при использовании раннера
  - Build
  - Release

variables:
  PACKAGE_REGISTRY_URL: "https://$DEPLOY_TOKEN_USERNAME:$DEPLOY_TOKEN_PASS@gitlab.b5a0.space/api/v4/projects/${CI_PROJECT_ID}/packages/generic/releases/${PACKAGE_VERSION}"

macos-Android-Debug-Build:
  stage: Build
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - chmod +x ./gradlew
    - ./gradlew assembleDebug
    - |
      curl --upload-file ./app/build/outputs/apk/debug/app-debug.apk "${PACKAGE_REGISTRY_URL}/macos-Android-Debug-Build"

windows-Android-Debug-Build:
  stage: Build
  when: manual
  tags:
    - windows
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - ./gradlew assembleDebug
    - $pair = "$($DEPLOY_TOKEN_USERNAME):$($DEPLOY_TOKEN_PASS)"
    - $encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
    - $basicAuthValue = "Basic $encodedCreds"
    - |
      $Headers = @{
        Authorization = $basicAuthValue
      }
    - Invoke-WebRequest -Uri $PACKAGE_REGISTRY_URL/windows-Android-Debug-Build -Headers $Headers -Method Put -InFile ./app/build/outputs/apk/debug/app-debug.apk

macos-Android-Release-Build:
  stage: Build
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - chmod +x ./gradlew
    - cd ./app
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
    - cd ../
    - ./gradlew :app:bundleRelease
    - |
      curl --upload-file ./app/build/outputs/bundle/release/app-release.aab "${PACKAGE_REGISTRY_URL}/macos-Android-Release-Build"

windows-Android-Release-Build:
  stage: Build
  when: manual
  tags:
    - windows
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - cd ./app
    - powershell -Command "& { (Invoke-WebRequest -Uri https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer.ps1 -UseBasicParsing).Content | Invoke-Expression }"
    - cd ../
    - ./gradlew :app:bundleRelease
    - $pair = "$($DEPLOY_TOKEN_USERNAME):$($DEPLOY_TOKEN_PASS)"
    - $encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
    - $basicAuthValue = "Basic $encodedCreds"
    - |
      $Headers = @{
        Authorization = $basicAuthValue
      }
    - Invoke-WebRequest -Uri $PACKAGE_REGISTRY_URL/windows-Android-Release-Build -Headers $Headers -Method Put -InFile ./app/build/outputs/bundle/release/app-release.aab

iOS-Simulator-Debug-Build:
  stage: Build
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    # Виводимо ім'я користувача
    - whoami
    # Виводимо робочу директорію
    - pwd
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо дебаг білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Debug
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/debugsimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug
    - zip -vr "debugsimulatorapp.zip" ./build/debugsimulatorapp.xcarchive
    - |
      curl --upload-file ./debugsimulatorapp.zip "${PACKAGE_REGISTRY_URL}/iOS-Simulator-Debug-Build"

iOS-Simulator-Release-Build:
  stage: Build
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    # Виводимо ім'я користувача
    - whoami
    # Виводимо робочу директорію
    - pwd
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо дебаг білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/releasesimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
    - zip -vr "releasesimulatorapp.zip" ./build/releasesimulatorapp.xcarchive
    - |
      curl --upload-file ./releasesimulatorapp.zip "${PACKAGE_REGISTRY_URL}/iOS-Simulator-Release-Build"

Release:
  stage: Release
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating script..."
  release:
    name: "Release $CI_COMMIT_TAG"
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
    description: "Release desription"
    assets:
      links:
        - name: windows-Android-Debug-Build
          url: $PACKAGE_REGISTRY_URL/windows-Android-Debug-Build
          link_type: package
        - name: windows-Android-Release-Build
          url: $PACKAGE_REGISTRY_URL/windows-Android-Release-Build
          link_type: package
        - name: macos-Android-Debug-Build
          url: $PACKAGE_REGISTRY_URL/macos-Android-Debug-Build
          link_type: package
        - name: macos-Android-Release-Build
          url: $PACKAGE_REGISTRY_URL/macos-Android-Release-Build
          link_type: package
        - name: iOS-Simulator-Debug-Build
          url: $PACKAGE_REGISTRY_URL/iOS-Simulator-Debug-Build
          link_type: package
        - name: iOS-Simulator-Release-Build
          url: $PACKAGE_REGISTRY_URL/iOS-Simulator-Release-Build
          link_type: package

```
