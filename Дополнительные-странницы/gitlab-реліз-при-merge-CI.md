```
# этапы pipline, разделяет логически работы на этапы, наприер для разных платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый раннер
  # Этап сборки проекта при использовании раннера
  - Create-Tag
  - Build
  - Release

variables:
  PACKAGE_REGISTRY_URL: "https://$DEPLOY_TOKEN_USERNAME:$DEPLOY_TOKEN_PASS@gitlab.b5a0.space/api/v4/projects/${CI_PROJECT_ID}/packages/generic/releases/${PACKAGE_VERSION}"
  # apps
  ANDROID_DEBUG_APP: ./app/build/outputs/apk/debug/app-debug.apk
  ANDROID_RELEASE_APP: ./app/build/outputs/bundle/release/app-release.aab
  IOS_DEBUG_APP: ./build/debugsimulatorapp.xcarchive
  IOS_RELEASE_APP: ./build/releasesimulatorapp.xcarchive

Create-Tag:
  stage: Create-Tag
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always
  script:
    - git tag $PACKAGE_VERSION
    - git remote | xargs -n1 git remote remove
    - git remote add gitlab_origin https://repository:$ACCESS_TOKEN_PASS@gitlab.b5a0.space/rn14/expo-test-app.git
    - git push gitlab_origin HEAD:$CI_DEFAULT_BRANCH --tags

MacOS-Android-Debug-Build:
  stage: Build
  tags:
    - android
  when: always
  rules:
    # if release and on default branch
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - chmod +x ./gradlew
    - ./gradlew assembleDebug
    - |
      curl --upload-file $ANDROID_DEBUG_APP "${PACKAGE_REGISTRY_URL}/android-debug-app"

Windows-Android-Debug-Build:
  stage: Build
  tags:
    - android
  when: manual
  rules:
    # if release and on default branch
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - ./gradlew assembleDebug
    - $pair = "$($DEPLOY_TOKEN_USERNAME):$($DEPLOY_TOKEN_PASS)"
    - $encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
    - $basicAuthValue = "Basic $encodedCreds"
    - |
      $Headers = @{
        Authorization = $basicAuthValue
      }
    - Invoke-WebRequest -Uri $PACKAGE_REGISTRY_URL/android-debug-app -Headers $Headers -Method Put -InFile $ANDROID_DEBUG_APP

MacOS-Android-Release-Build:
  stage: Build
  tags:
    - android
  when: always
  rules:
    # if release and on default branch
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - chmod +x ./gradlew
    - cd ./app
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
    - cd ../
    - ./gradlew :app:bundleRelease
    - |
      curl --upload-file $ANDROID_RELEASE_APP "${PACKAGE_REGISTRY_URL}/android-release-app"

Windows-Android-Release-Build:
  stage: Build
  tags:
    - android
  when: manual
  rules:
    # if release and on default branch
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    - cd android
    - cd ./app
    - powershell -Command "& { (Invoke-WebRequest -Uri https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer.ps1 -UseBasicParsing).Content | Invoke-Expression }"
    - cd ../
    - ./gradlew :app:bundleRelease
    - $pair = "$($DEPLOY_TOKEN_USERNAME):$($DEPLOY_TOKEN_PASS)"
    - $encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
    - $basicAuthValue = "Basic $encodedCreds"
    - |
      $Headers = @{
        Authorization = $basicAuthValue
      }
    - Invoke-WebRequest -Uri $PACKAGE_REGISTRY_URL/android-release-app -Headers $Headers -Method Put -InFile $ANDROID_RELEASE_APP

IOS-Simulator-Debug-Build:
  stage: Build
  tags:
    - ios
    # if release and on default branch
  when: always
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    # Виводимо ім'я користувача
    - whoami
    # Виводимо робочу директорію
    - pwd
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо дебаг білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Debug
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/debugsimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug
    - zip -vr "debugsimulatorapp.zip" $IOS_DEBUG_APP
    - |
      curl --upload-file ./debugsimulatorapp.zip "${PACKAGE_REGISTRY_URL}/ios-simulator-debug-app"

IOS-Simulator-Release-Build:
  stage: Build
  tags:
    - ios
  when: always
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - npm install
    # Виводимо ім'я користувача
    - whoami
    # Виводимо робочу директорію
    - pwd
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо дебаг білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/releasesimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
    - zip -vr "releasesimulatorapp.zip" $IOS_RELEASE_APP
    - |
      curl --upload-file ./releasesimulatorapp.zip "${PACKAGE_REGISTRY_URL}/ios-simulator-release-app"

Release:
  stage: Release
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating script..."
  release:
    name: "Release $CI_COMMIT_TAG"
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
    description: "Release desription"
    assets:
      links:
        - name: android-debug-app
          url: $PACKAGE_REGISTRY_URL/android-debug-app
          link_type: package
        - name: android-release-app
          url: $PACKAGE_REGISTRY_URL/android-release-app
          link_type: package
        - name: ios-simulator-debug-app
          url: $PACKAGE_REGISTRY_URL/ios-simulator-debug-app
          link_type: package
        - name: ios-simulator-release-app
          url: $PACKAGE_REGISTRY_URL/ios-simulator-release-app
          link_type: package

```
