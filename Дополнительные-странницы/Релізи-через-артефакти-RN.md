```
# этапы pipline, разделяет логически работы на этапы, наприер для разных платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый раннер
  # Этап сборки проекта при использовании раннера
  - BuildAndRelease

variables:
  ARTIFACTS_DOWNLOAD_URL: https://gitlab.b5a0.space/rn14/test-app/-/jobs/$CI_JOB_ID/artifacts/download
  #
  IOS_DEBUG_ARCHIVE_PATH: ios/build/debugtestapp.xcarchive
  IOS_RELEASE_ARCHIVE_PATH: ios/build/releasetestapp.xcarchive
  ANDROID_DEBUG_APP_PATH: android/app/build/outputs/apk/debug/app-debug.apk
  ANDROID_RELEASE_APP_PATH: android/app/build/outputs/bundle/release/app-release.aab

Artifacts-Build-And-Release:
  stage: BuildAndRelease
  when: manual
  tags:
    - macos
  rules:
    - if: $CI_COMMIT_TAG
  script:
    # Виводимо ім'я користувача
    - whoami
    - echo 'Android debug app building...'
    - cd android
    - chmod +x ./gradlew
    - ./gradlew assembleDebug
    - ./gradlew --stop
    - echo 'Android release app building...'
    - cd app
    - curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash
    - cd ../
    - ./gradlew :app:bundleRelease
    - ./gradlew --stop
    - echo 'iOS debug app for simulator building...'
    - cd ../
    # генеруємо нативний код для проекту
    - npx expo prebuild
    # переходимо в директорію ios
    - cd ios
    # встановлюємо залежності в ios
    - pod install
    # робимо реліз білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace testapp.xcworkspace -scheme testapp -destination 'generic/platform=iOS Simulator' -configuration Debug
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/debugtestapp -workspace testapp.xcworkspace -scheme testapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug
      # робимо реліз білд для iOS симулятора
    # для повної документації xcodebuild команди пропишіть в терміналі xcodebuild --help. Для основної(скороченої) документації xcodebuild --usage.
    # build - параметр який визначає дію xcodebuild. Замість build можливі варіанти: archive, test, install та інші
    # CODE_SIGN_STYLE - метод для визначення і вставки підпису. Можливі варіанти: Automatic, Manual
    # DEVELOPMENT_TEAM - id розробницької команди, використовується для сертифікатів підпису і профілів надання(signing certificates і provisioning profiles)
    # -destination - для какой платформы собираем. Может быть 'generic/platform=iOS'
    # -configuration - может быть Debug или Release
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace testapp.xcworkspace -scheme testapp -destination 'generic/platform=iOS Simulator' -configuration Release
    # архівуємо реліз білд
    - xcodebuild archive -quiet -archivePath ./build/releasetestapp -workspace testapp.xcworkspace -scheme testapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
  artifacts:
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 days
    # условия при которм артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаються артефакты, в архиве будет находится то, что указано в пути
    paths:
      - $IOS_DEBUG_ARCHIVE_PATH
      - $IOS_RELEASE_ARCHIVE_PATH
      - $ANDROID_DEBUG_APP_PATH
      - $ANDROID_RELEASE_APP_PATH
  release:
    name: "Release $CI_COMMIT_TAG"
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
    description: "Release desription"
    assets:
      links:
        - name: apps
          url: $ARTIFACTS_DOWNLOAD_URL
          link_type: package

```
