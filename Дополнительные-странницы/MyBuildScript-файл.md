```
using CAS;
using CAS.UEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEditor;
using UnityEditor.Android;
using UnityEditor.Build;
using UnityEditor.Build.Content;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyBuildScript: IPreprocessBuildWithReport {
  public int callbackOrder => 0;

  private
  const string initialVersion = "0.0";
  public void OnPreprocessBuild(BuildReport report) {

  }
  public static void MyBuildProcess() {
    var settings = UpdateSettings();

    // Переменна являющаяся ID проекта, убедится, что совпадает с зарегистрированным ID, который указан в README проекта
    PlayerSettings.applicationIdentifier = GetEnvironmentVariable("ID");
    PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, GetEnvironmentVariable("ID"));
    PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, GetEnvironmentVariable("ID"));
    //Bundle version for Android and iOS

    // переменные для активации keystore, убедится, что совпадает с информацией из README проекта
    PlayerSettings.Android.keystoreName = "./my.keystore";
    PlayerSettings.Android.keystorePass = "myPassword";
    PlayerSettings.Android.keyaliasName = "business";
    PlayerSettings.Android.keyaliasPass = "myPassword";

    //Android Settings
    //Если нет доступа к Setting->CI/CD->Variables использовать [хардкод](///III.-Дополнительные-пункты/Глоссарий#hardcode) вариант

    //Bundle version code
    PlayerSettings.bundleVersion = GetEnvironmentVariable("BUNDLEVERSIONCODEANDROID");
    //MinimumAPILevel
    PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel22;
    //TargetAPILevel
    PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;

    if (GetEnvironmentVariable("ANDROIDNAME").Split('.')[1] == "aab") {
      EditorUserBuildSettings.buildAppBundle = true;
    } else {
      EditorUserBuildSettings.buildAppBundle = false;
    }

    //iOS Settings
    //Build
    PlayerSettings.iOS.buildNumber = GetEnvironmentVariable("BUILDIOS");
    //Target minimum iOS version
    PlayerSettings.iOS.targetOSVersionString = GetEnvironmentVariable("TARGETVERSION");
    //Target Device
    PlayerSettings.iOS.targetDevice = iOSTargetDevice.iPhoneAndiPad;
    //Device SDK
    PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;

    var buildPostfix = settings.commitRef.Replace("/", "_");
    var buildPrefix = string.Format("{0}_{1}", GetEnvironmentVariable("BUILD_PREFIX"), buildPostfix);
    var buildTarget = (BuildTarget) Enum.Parse(typeof (BuildTarget), GetEnvironmentVariable("BUILD_TARGET"));
    // сбор всех сцен на проекте             
    string[] scenes = GetScenes();
    string path = null;
    switch (buildTarget) {
    case BuildTarget.iOS:
      //Путь создания xCode проекта для iOS
      path = Path.Combine(GetEnvironmentVariable("OUTPUT_DIR"), buildPrefix);
      break;
    case BuildTarget.Android:
      //Путь создания установщика для android, последний параметр функции должен содержать название проекта
      path = Path.Combine(GetEnvironmentVariable("OUTPUT_DIR"), buildPrefix, GetEnvironmentVariable("ANDROIDNAME"));
      break;
    default:
      //TODO error handling
      return;
    }
    var report = BuildPipeline.BuildPlayer(scenes, path, buildTarget, BuildOptions.None);
    if (Application.isBatchMode) {
      EditorApplication.Exit(report.summary.totalErrors > 0 ? 1 : 0);
    }
  }
  // Функция для сохранения информации о автосборке, в MySettings.asset сохраняется информация о pipeline, job, commit
  private static MySettings UpdateSettings() {
    //Custom settings
    var settings = AssetDatabase.LoadAssetAtPath < MySettings > ("Assets/Resources/MySettings.asset");
    settings.commitRef = GetEnvironmentVariable("CI_COMMIT_REF_NAME");
    settings.jobId = GetEnvironmentVariable("CI_JOB_ID");
    settings.pipelineId = GetEnvironmentVariable("CI_PIPELINE_ID");
    settings.commitHash = GetEnvironmentVariable("CI_COMMIT_SHA");
    settings.commitMessage = GetEnvironmentVariable("CI_COMMIT_MESSAGE");
    settings.backendBaseURL = GetEnvironmentVariable("BACKEND_BASE_URL");
    EditorUtility.SetDirty(settings);
    AssetDatabase.SaveAssets();
    //Player settings
    var tag = GetEnvironmentVariable("CI_COMMIT_TAG");
    PlayerSettings.bundleVersion = string.IsNullOrWhiteSpace(tag) ? "1.0.0" : tag;
    return settings;
  }

  private static string GetEnvironmentVariable(string name) {
    return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
  }

  private static void SetEnvironmentVariable(string name, string value) {
    Environment.SetEnvironmentVariable(name, value, EnvironmentVariableTarget.Process);
  }

  // функции для добавления всех сцен в проекте в один массив
  static string[] GetScenes() {
    var projectScenes = EditorBuildSettings.scenes;
    List < string > scenesToBuild = new List < string > ();
    for (int i = 0; i < projectScenes.Length; i++) {
      if (projectScenes[i].enabled) {
        scenesToBuild.Add(projectScenes[i].path);
      }
    }
    return scenesToBuild.ToArray();
  }
}
```