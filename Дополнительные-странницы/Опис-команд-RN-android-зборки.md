Для android зборки react-native проекту використовувати наступні команди:

1. ```> chmod +x gradlew```. Команда доступна на macOS i Linux, надає право ```gradlew``` виконуватись

2. ```> ./gradlew assembleDebug```. Команда для створення дебаг android зборки

3. ```> ./gradlew :app:bundleRelease```. Команда для створення реліз android зборки