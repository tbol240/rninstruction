```
const { task, series } = require("gulp");
const child_process = require("child_process");
const path = require("path");

// проверка на каком устройстве работает runner Windows или macOS
const isWindows = process.platform === "win32";
let _unityPath = null;

// Функция поиска Unity Editor
const findUnity = function () {
  // Версия Unity указанная в файле .gitlab-ci.yml
  const unityVersion = process.env.UNITY_VERSION;
  // Место нахождения Unity Hub
  let unityHubPath = process.env.UNITY_HUB_PATH;
  // использования нужного пути в зависимости от операционной сисимы устройства
  let unityPath = isWindows
    ? process.env.UNITY_LOCATION_WINDOWS
    : process.env.UNITY_LOCATION_MACOS;
  // В случае если путь к Unity Hub не указан, используется путь по-умолчанию
  if (!unityHubPath) {
    unityHubPath = isWindows
      ? "C:/Program Files/Unity Hub/Unity Hub.exe"
      : "/Application/Unity Hub.app/Contents/MacOS/Unity Hub";
  }
  // запуск Unity Hub для активации лицензии Unity
  const data = child_process.spawnSync(
    unityHubPath,
    ["--", "--headless", "editors", "-i"],
    { encoding: "utf8" }
  );
  // Проверка на наличие правильного пути к Unity Editor
  console.log("Variable path:", unityPath);
  const candidate = unityPath;
  if (!candidate) {
    return Promise.reject(`Could not find unity ${unityVersion}`);
  }

  _unityPath = candidate.split("installed at ")[1];
  console.log("unity path:", _unityPath);
  // Отделение самого Editor для сборки
  return Promise.resolve(_unityPath);
};

const buildUnity = function () {
  //https://docs.unity3d.com/Manual/CommandLineArguments.html
  // Указания на каких платформах делать сборку
  const buildTargets = {
    Android: "Android",
    iOS: "iOS",
  };

  const projectPath = path.resolve(process.cwd());

  const escapedProjectPath = isWindows
    ? `"${projectPath}"`
    : projectPath.replace(/\s/g, "\\ ");

  // запуск Unity для сборки и запуск скрипта MyBuildScript для настроек сборки

  const unityEditorProcess = child_process.spawn(
    _unityPath,
    [
      "-batchmode",
      "-quit",
      "-silent-crashes",
      "-logfile",
      "-", //log to std out
      "-projectPath",
      "./",
      "-CacheServerIPAddress",
      process.env.UNITY_CACHE_SERVER,
      "-buildTarget",
      buildTargets[process.env.BUILD_TARGET],
      "-executeMethod",
      "MyBuildScript.MyBuildProcess",
    ],
    {
      cwd: projectPath,
      windowsHide: true,
      label: "Unity",
      env: process.env,
    }
  );

  unityEditorProcess.stdout.pipe(process.stdout);
  unityEditorProcess.stderr.pipe(process.stderr);
  return unityEditorProcess;
};

task("buildWindows", series(findUnity, buildUnity));

task("default", findUnity);
```