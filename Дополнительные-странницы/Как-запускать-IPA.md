 
# Как запускать [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa) файл
[IPA](///III.-Дополнительные-пункты//Глоссарий#ipa) файл можно запускать через: 
1. [Симулятор](#запуск-ios-приложения-через-симулятор)
2. [IOS устройство](#запуск-ios-приложения-через-реальное-устройство)

## Запуск [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa) файла через Симулятор

### Симулятор [XCode](///III.-Дополнительные-пункты//Глоссарий#xcode)
1. Открыть проект в <b>[XCode](///III.-Дополнительные-пункты//Глоссарий#xcode)</b>.
2. Выбирать главный раннер во вкладке проектов.

[<img src="///Images//BuildAndRun/select-the-main-runner_censored.jpg" width = "500">](///Images//BuildAndRun/select-the-main-runner_censored.jpg)

3. Выбрать симулятор на котором нужно запустить [приложение](///III.-Дополнительные-пункты//Глоссарий#приложение).

[<img src="///Images//BuildAndRun/select-a-device_censored.jpg" width = "500">](///Images//BuildAndRun/select-a-device_censored.jpg)

4. Нажать на кнопку <b>Run</b>.

[<img src="///Images//BuildAndRun/press-the-run-button_censored.jpg" width = "500">](///Images//BuildAndRun/press-the-run-button_censored.jpg)

### Симулятор [MacOS](///III.-Дополнительные-пункты//Глоссарий#macos) с Apple Silicon
1. Найти/создать [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa)-файл следуя [инструкции](#как-получить-ipa-файл).
2. Запустить [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa)-файл.


## Запуск [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa) файла через [IOS](///III.-Дополнительные-пункты//Глоссарий#ios) устройство
1. Подключить [IOS](///III.-Дополнительные-пункты//Глоссарий#ios) устройство через кабель к [МacOS](///III.-Дополнительные-пункты//Глоссарий#macos).
2. В <b>[XCode](///III.-Дополнительные-пункты//Глоссарий#xcode)</b> в верхнем меню выбрать <b>Window Devices and Simulators</b>.

[<img src="///Images//BuildAndRun/window-device-simulator_censored.jpg" width = "500">](///Images//BuildAndRun/window-device-simulator_censored.jpg)

4. Нажать на вкладку <b>Devices</b>.

[<img src="///Images//BuildAndRun/devices-tab_censored.jpg" width = "500">](///Images//BuildAndRun/devices-tab_censored.jpg)

5. Выбрать устройство, подключенное через USB из списка.

[<img src="///Images//BuildAndRun/select-connected-device_censored.jpg" width = "500">](///Images//BuildAndRun/select-connected-device_censored.jpg)

6. В разделе <b>Apps</b> нажать на кнопку <b>+</b>.

[<img src="///Images//BuildAndRun/press-plus-button-to-add-app_censored.jpg" width = "500">](///Images//BuildAndRun/press-plus-button-to-add-app_censored.jpg)

7. Найти/создать [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa)-файл следуя [инструкции](#как-получить-ipa-файл).


## Как получить [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa)-файл

1. Открыть проект в [XCode](///III.-Дополнительные-пункты//Глоссарий#xcode).
2. Открыть окно Organizer, нажав Window > Organizer, а затем выбрать вкладку Archives.
3. Выбрать нужный архив, нажать кнопку <b>Distribute App</b>, в окне <b>Select method of distibution</b> выбрать <b>Ad Hoc</b>.

[<img src="///Images//BuildAndRun/select-ad-hoc_censored.jpg" width = "500">](///Images//BuildAndRun/select-ad-hoc_censored.jpg)

- В окне <b>Ad Hoc distribution options</b> ничего не менять и нажать кнопку <b>Next</b>.

[<img src="///Images//BuildAndRun/adhoc-options_censored.jpg" width = "500">](///Images//BuildAndRun/adhoc-options_censored.jpg)

- В окне <b>Re-sign</b> ничего не менять и нажать кнопку <b>Next</b>.

[<img src="///Images//BuildAndRun/resign_censored.jpg" width = "500">](///Images//BuildAndRun/resign_censored.jpg)

- В окне <b>Review</b> нужно нажать кнопку <b>Export</b> и выбрать место в файловой системе, куда экспортировать [приложение](///III.-Дополнительные-пункты//Глоссарий#приложение).

[<img src="///Images//BuildAndRun/review-adhoc_censored.jpg" width = "500">](///Images//BuildAndRun/review-adhoc_censored.jpg)

## Как получить дебаг [IPA](///III.-Дополнительные-пункты//Глоссарий#ipa)-файл

1. Открыть файл `.xcarchive` который был создан после выполнения [инструкции](II.-Техническая-(справочная)-инструкции/Запуск-зборки-проекту)

[<img src="///Images//BuildAndRun/xcarhive_file.png" width = "500">](///Images//BuildAndRun/xcarhive_file.png)

2. Нажать кнопку `Distribute App`

[<img src="///Images//BuildAndRun/distribute_app.png" width = "500">](///Images//BuildAndRun/distribute_app.png)

3. Выбрать метод Debugging

[<img src="///Images//BuildAndRun/debug_method.png" width = "500">](///Images//BuildAndRun/debug_method.png)

4. Нажать `Export`, выбрать путь куда сохранится файл, и еще раз нажать `Export`

[<img src="///Images//BuildAndRun/export_button.png" width = "500">](///Images//BuildAndRun/export_button.png)

5. Полученый `.ipa` файл

[<img src="///Images//BuildAndRun/ipa_file_in_folder.png" width = "500">](///Images//BuildAndRun/ipa_file_in_folder.png)