```
# Этапы pipeline, разделяет логически работы на этапы, например для разных платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый runner
  # Этап сборки проекта unity при использовании runner-а
  - UnityBuild

# Переменные, используемые на протяжении всей сборки, указываются в скриптах. (https://docs.gitlab.com/ee/ci/yaml/#variables)
variables:
  # Переменная устанавливающая стандарт кодирования, нужна для нормально отображение логов
  LC_ALL: C.UTF-8
  # Переменная указывающая названия папки в которую собирается build
  OUTPUT_DIR: "Builds"

  # Номер версии, на которой происходит сборка, менять в зависимости от используемой версии Unity
  # UNITY_VERSION: "2021.3.32f1" # хардкод строка, альтернатива Settings->CI/CD->Variables
  UNITY_VERSION: UNITY_VERSION # Альтернатива
  # Хардкод версия доступна для разработчиков с правами developers
  # Альтернатива доступна разработчикам с правами maintainers

  # Пути к Unity Editor на windows и macOS, нужны для подтверждения версии и запуска самого едитора для начала сборки
  # Путь к Unity Editor-у для windows, нужно быть уверенным, что Unity Editor установлен и находится на указанном пути, для windows скорее всего нужно будет менять путь для разных устройств, на которых будет работать runner
  # UNITY_LOCATION_WINDOWS : "2021.3.26f1 , installed at G:\\Unity\\2021.3.26f1\\Editor\\Unity.exe" # хардкод строка, альтернатива Settings->CI/CD->Variables
  UNITY_LOCATION_WINDOWS: $UNITY_LOCATION_WINDOWS # Альтернатива
  # Хард кор версия доступна для разработчиков с правами developers
  # Альтернатива доступна разработчикам с правами maintainers

  # Путь к Unity Editor-у для macOS, нужно быть уверенным, что Unity Editor установлен и находится на указанном пути,скорее всего для macOS не нужно будет менять путь, установка на них происходит обычно в одно и тоже место
  # UNITY_LOCATION_MACOS : "2021.3.32f1 , installed at /Applications/Unity/Hub/Editor/2021.3.26f1/Unity.app/Contents/MacOS/Unity" # хардкод строка, альтернатива Settings->CI/CD->Variables
  UNITY_LOCATION_MACOS: $UNITY_LOCATION_MACOS # Альтернатива
  # Хардкод версия доступна для разработчиков с правами developers
  # Альтернатива доступна разработчикам с правами maintainers

  # Ідентифікатор Unity застосунку. Змінна викорисовується в файлі Assets/Editor/MyBuildScript.cs
  ID: $Project_ID
  # Версія Android бандлу Unity застосунку. Змінна викорисовується в файлі Assets/Editor/MyBuildScript.cs
  # Інкрементувати кожен раз при сборці на 1. Не можливо викласти в Play Store застосунок з поточною або старою версією android бандлу
  BUNDLEVERSIONCODEANDROID: $Bundle_Version_Code
  # Версія iOS бандлу Unity застосунку. Змінна викорисовується в файлі Assets/Editor/MyBuildScript.cs
  # Інкрементувати кожен раз при сборці на 1. Не можливо викласти в App Store застосунок з поточною або старою версією iOS бандлу
  BUILDIOS: $Build_iOS
  # Мінімальна версія iOS таргету. Змінна викорисовується в файлі Assets/Editor/MyBuildScript.cs
  TARGETVERSION: $Target_SDK
  # Назва інсталятора для android. Має мати назву проекту. Змінна викорисовується в файлі Assets/Editor/MyBuildScript.cs
  ANDROIDNAME: $Android_Install_Name
  # Сервер для сбора кеша, изменение в данной переменной не требуются
  UNITY_CACHE_SERVER: "localhost:8126"

# Глобальное ключевое слово, позволяющее установить глобальное значение для других ключевых слов (https://docs.gitlab.com/ee/ci/yaml/#default)
default:
  # Используйте before_script, чтобы определить массив команд, которые должны выполняться перед командами сценария каждого задания, но после восстановления артефактов.(https://docs.gitlab.com/ee/ci/yaml/#before_script)
  before_script:
    # установка необходимых для сборки пакетов
    - npm i

# Используйте cache, чтобы указать список файлов и каталогов для кэширования между заданиями. Вы можете использовать только пути, находящиеся в локальной рабочей копии. (https://docs.gitlab.com/ee/ci/yaml/#cache)
cache:
  # Параметр job, который указывает путь для создания файлов в процессе выполнение job
  paths:
    - node_modules/

# job для сборки проекта для релиза на Android
Android:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленных в stages
  stage: UnityBuild
  # Параметр job, указывающий на условия начала работы, может быть при указании тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой runner может брать эту работу. Используются для избежания ситуаций, когда например Windows пытается собрать iOS build без xCode
  tags:
    - android
  # Используйте cache, чтобы указать список файлов и каталогов для кэширования между заданиями. Вы можете использовать только пути, находящиеся в локальной рабочей копии. (https://docs.gitlab.com/ee/ci/yaml/#cache)
  cache:
    # По указанному ключу происходит разделения кеширования
    key: windows
    # Використовуйте ключове слово cache:paths, щоб вибрати, які файли чи каталоги кешувати.
    paths:
      - Library/
  # Переменные указанные отдельно только в job используются в ней и в вызванных ею скриптах
  variables:
    # переменная, указывающая unity на какую платформу будет собираться build
    BUILD_TARGET: "Android"
    BACKEND_BASE_URL: "https://production.mydomain.com/mygame"
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # команда для запуска спорки проекти, вызывает функцию buildWindows из файла gulpfile.js
    - npx gulp buildWindows
  # artifacts - файлы созданные job в процессе работы, они выгружаются по указанному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: production-windows-build
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при котором артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаются артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ./Builds/**

# Job iOS сборки. Запускати якщо встановлений pod і з ним немає проблем
iOSPodNormal:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленных в stages
  stage: UnityBuild
  # Параметр job, указывающий на условия начала работы, может быть при указании тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой runner может брать эту работу. Используются для избежания ситуаций, когда например Windows пытается собрать iOS build без xCode
  tags:
    - ios
    - podnormal
  only:
    - pushes
  # Используйте cache, чтобы указать список файлов и каталогов для кэширования между заданиями. Вы можете использовать только пути, находящиеся в локальной рабочей копии. (https://docs.gitlab.com/ee/ci/yaml/#cache)
  cache:
    # По указанному ключу происходит разделения кеширования
    key: windows
    # Використовуйте ключове слово cache:paths, щоб вибрати, які файли чи каталоги кешувати.
    paths:
      - Library/
  # Переменные указанные отдельно только в job используются в ней и в вызванных ею скриптах
  variables:
    # переменная, указывающая unity на какую платформу будет собираться build
    BUILD_TARGET: "iOS"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
    ID: $Project_ID
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # ім'я поточного юзера
    - whoami
    # команда для запуска спорки проекти, вызывает функцию buildWindows из файла gulpfile.js
    - npx gulp buildWindows
    # команада, що виводить поточну версію ruby
    - ruby -v
    # команда що змінює директорію на ту де білд
    - cd ./Builds/_$CI_COMMIT_BRANCH

    #- mkdir -p $HOME/Software/ruby
    #- export GEM_HOME=$HOME/Software/ruby
    #- gem install cocoapods
    #- export PATH=$PATH:$HOME/Software/ruby/bin

    # команда що змінює директорію на ту де білд
    - cd $CI_PROJECT_DIR/Builds/_$CI_COMMIT_BRANCH
    # вказуємо змінну оточення локалізація для macOS
    - export LC_ALL=en_US.UTF-8
    - export LANG=en_US.UTF-8
    - export LANGUAGE=en_US.UTF-8
    - export LC_ALL=en_US.UTF-8
    # команда яка створює Podfile
    - pod init
    # записуємо значення змінної оточення POD в Podfile
    - echo -e $POD > Podfile
    # встановлюємо всі залежності визначені в Podfile
    - pod install
    # створюємо файл ExportOptions.plist
    - touch ExportOptions.plist
    # записуємо значення змінної оточення в файл ExportOptions.plist
    - echo -e $ExportOptions > ExportOptions.plist
    # Детальний опис Xcode сборки через термінал: https://gitlab.b5a0.space/internal/instruction/-/wikis/%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%BD%D0%B8%D1%86%D1%8B/Xcode-%D1%81%D0%B1%D0%BE%D1%80%D0%BA%D0%B0-%D1%87%D0%B5%D1%80%D0%B5%D0%B7-%D1%82%D0%B5%D1%80%D0%BC%D1%96%D0%BD%D0%B0%D0%BB
    # створюємо білд через XCode
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Unity-iPhone.xcworkspace -scheme Unity-iPhone -destination 'generic/platform=iOS' -configuration Release -allowProvisioningUpdates
    # архівуємо білд
    - xcodebuild -quiet archive -archivePath build/Unity-iPhone -workspace Unity-iPhone.xcworkspace -scheme Unity-iPhone -destination 'generic/platform=iOS' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release -allowProvisioningUpdates
    # перетворюємо архів на .ipa файл
    - xcodebuild -quiet -exportArchive -archivePath 'build/Unity-iPhone.xcarchive' -exportPath "./" -exportOptionsPlist "./ExportOptions.plist" -destination 'generic/platform=iOS' CODE_SIGN_STYLE=Automatic
    # валідація білда
  # artifacts - файлы созданные job в процессе работы, они выгружаются по указанному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: production-windows-build
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при котором артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаются артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ./Builds/**

# Job iOS сборки. Запускати якщо не встановлений pod
iOS:
  # Параметр job, указывающий на коком stage будет данная работа, указать можно любую из перечисленных в stages
  stage: UnityBuild
  # Параметр job, указывающий на условия начала работы, может быть при указании тега(tag), всегда(always), или как в данном случаи при ручном запуске разработчиком(manual), до тех пор, пока разработчик сам не запустит job, он будет иметь статус пропущенный (skipped)
  when: manual
  # Теги для указание какой runner может брать эту работу. Используются для избежания ситуаций, когда например Windows пытается собрать iOS build без xCode
  tags:
    - ios
  only:
    - pushes
  # Используйте cache, чтобы указать список файлов и каталогов для кэширования между заданиями. Вы можете использовать только пути, находящиеся в локальной рабочей копии. (https://docs.gitlab.com/ee/ci/yaml/#cache)
  cache:
    # По указанному ключу происходит разделения кеширования
    key: windows
    # Використовуйте ключове слово cache:paths, щоб вибрати, які файли чи каталоги кешувати.
    paths:
      - Library/
  # Переменные указанные отдельно только в job используются в ней и в вызванных ею скриптах
  variables:
    # переменная, указывающая unity на какую платформу будет собираться build
    BUILD_TARGET: "iOS"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
    ID: $Project_ID
  # cкрипт вызываемый job, записывать по строчно команды для консоли
  script:
    # ім'я поточного юзера
    - whoami
    # команда для запуска спорки проекти, вызывает функцию buildWindows из файла gulpfile.js
    - npx gulp buildWindows
    # команада, що виводить поточну версію ruby
    - ruby -v
    # команда що змінює директорію на ту де білд
    - cd ./Builds/_$CI_COMMIT_BRANCH
    - mkdir -p $HOME/Software/ruby
    - export GEM_HOME=$HOME/Software/ruby
    - gem install cocoapods
    - export PATH=$PATH:$HOME/Software/ruby/bin
    # команда що змінює директорію на ту де білд
    - cd $CI_PROJECT_DIR/Builds/_$CI_COMMIT_BRANCH
    # вказуємо змінні оточення локалізація для macOS
    - export LC_ALL=en_US.UTF-8
    - export LANG=en_US.UTF-8
    - export LANGUAGE=en_US.UTF-8
    # команда яка створює Podfile
    - pod init
    # записуємо значення змінної оточення POD в Podfile
    - echo -e $POD > Podfile
    # встановлюємо всі залежності визначені в Podfile
    - pod install
    # створюємо файл ExportOptions.plist
    - touch ExportOptions.plist
    # записуємо значення змінної оточення в файл ExportOptions.plist
    - echo -e $ExportOptions > ExportOptions.plist
    # Детальний опис Xcode сборки через термінал: https://gitlab.b5a0.space/internal/instruction/-/wikis/%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%BD%D0%B8%D1%86%D1%8B/Xcode-%D1%81%D0%B1%D0%BE%D1%80%D0%BA%D0%B0-%D1%87%D0%B5%D1%80%D0%B5%D0%B7-%D1%82%D0%B5%D1%80%D0%BC%D1%96%D0%BD%D0%B0%D0%BB
    # створюємо білд через XCode
    - xcodebuild build CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace Unity-iPhone.xcworkspace -scheme Unity-iPhone -destination 'generic/platform=iOS' CODE_SIGN_IDENTITY='Apple Development'
    # архівуємо білд
    - xcodebuild archive -archivePath build/Unity-iPhone -workspace Unity-iPhone.xcworkspace -scheme Unity-iPhone -destination 'generic/platform=iOS' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V CODE_SIGN_IDENTITY='Apple Development'
    # перетворюємо архів на .ipa файл
    - xcodebuild -exportArchive -archivePath build/Unity-iPhone.xcarchive -exportPath "./" -exportOptionsPlist "ExportOptions.plist" -destination generic/platform=iOS CODE_SIGN_STYLE=Automatic
    # валідація білда
    - xcrun altool --validate-app -f BabyCare.ipa -t ios -u $APPLE_ID -p gddd-gjhm-vtzc-ngtu
    # білд викладається на App Store Connect
    - xcrun altool --upload-app -f BabyCare.ipa -t ios -u b2d0@proton.me -p gddd-gjhm-vtzc-ngtu
  # artifacts - файлы созданные job в процессе работы, они выгружаются по указанному пути и могут быть скачаны в виде архива, это и есть результат сборки
  artifacts:
    # Имя архива артефактов
    name: production-windows-build
    # Время, через которое артефакты будут удалены с сервера
    expire_in: 2 hrs
    # условия при котором артефакты будут собраны в архив и предложены для скачивания
    when: on_success
    # путь по которому выгружаются артефакты, в архиве будет находится то, что указано в пути
    paths:
      - ./Builds/**
```
