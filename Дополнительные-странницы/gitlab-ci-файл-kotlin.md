```
# етапи pipline, розділяє логічно роботи на етапи, наприклад для різних платформ (https://docs.gitlab.com/ee/ci/yaml/#stages)
stages:
  # Обирати етап складання потрібно виходячи з того, на якому пристрої запущено runner, що використовується
  # Етап складання проєкту під час використання раннера
  - AndroidStudioBuild

#змінні які будуть використані при створенні build
variables:
  ANDROID_COMPILE_SDK: "33"
  ANDROID_BUILD_TOOLS: "33.0.0"
  GRADLE_USER_HOME: "$PWD/.gradle"

# job для складання проекту для дебага на Android
AndroidDebug:
# Параметр job, що вказує на якому stage буде дана робота, вказати можна будь-яку з перерахованих у stages
  stage: Build
# скрипт, що викликається job, записує по рядку команди для консолі
  script:
    - ./gradlew assembleDebug
    - ./gradlew --stop
# Теги для вказівки який runner може брати цю job. 
  tags:
    - android
# artifacts - файли, створені job у процесі роботи, вони вивантажуються за вказаним шляхом і можуть бути викачані у вигляді архіву, це і є результат складання
  artifacts:
# Ім'я архіву артефактів
    name: debug-build
# шлях, за яким вивантажуються artifacts, в архіві буде знаходиться те, що вказано в шляху
    paths:
      - app/build/outputs/apk/debug/app-debug.apk

# job для складання проекту релізної версії на Android
AndroidRelease:
# Параметр job, що вказує на якому stage буде дана робота, вказати можна будь-яку з перерахованих у stages
  stage: Build
# скрипт, що викликається job, записує по рядку команди для консолі
  script:
    - ./gradlew assembleRelease
    - ./gradlew --stop
# Теги для вказівки який runner може брати цю job. 
  tags:
    - android
# artifacts - файли, створені job у процесі роботи, вони вивантажуються за вказаним шляхом і можуть бути викачані у вигляді архіву, це і є результат складання
  artifacts:
# Ім'я архіву артефактів
    name: release-build
# шлях, за яким вивантажуються artifacts, в архіві буде знаходиться те, що вказано в шляху
    paths:
      - app/build/outputs/apk/release/app-release.apk
```