```
# этапы pipline, разделяет логически работы на этапы, наприер для разных платворм (https://docs.gitlab.com/ee/ci/yaml/#stages)

stages:

# Выбирать этап спорки нужно исходя из того, на каком устройстве запущен используемый раннер

# Этап сборки проекта unity при использовании раннера

- Build
- Release

# Переменные, используемые на протяжении всей сборки, указываються в скриптах. (https://docs.gitlab.com/ee/ci/yaml/#variables)

variables:

# Переменная устанвлювающая стандарт кодирования, нужна для нормально отображение логов

LC_ALL: C.UTF-8

# Переменная указывающая названия папки в которую собирается билд

OUTPUT_DIR: "Builds"

# Номер версии, на которой происходит сборка, менять в зависимости от используемой версии Юнити

# UNITY_VERSION: "2021.3.26f1" # Хард код строка, алтернатива Settings->CI/CD->Variables

UNITY_VERSION: $UNITY_VERSION # Альтернатива

# Хард кор версия доступна для разработчиков с правами developers

# Альтернатива доступна разработчикам с правами maintainers

# Пути к Юнити Едитору на windows и macOS, нужны для подтверждения версии и запуска самого едитора для начала сборки

#Путь к юнити едитору для windows, нужно быть уверенным, что едитор установлен и находится на указаном пути, для windows скорее всего нужно будет менять путь для разных устройств, на которых будет работать раннер

# UNITY_LOCATION_WINDOWS : "2021.3.26f1 , installed at G:\\Unity\\2021.3.26f1\\Editor\\Unity.exe" # Хард код строка, алтернатива Settings->CI/CD->Variables

UNITY_LOCATION_WINDOWS: $UNITY_LOCATION_WINDOWS

# Альтернатива

# Хард кор версия доступна для разработчиков с правами developers

# Альтернатива доступна разработчикам с правами maintainers

#Путь к юнити едитору для macOS, нужно быть уверенным, что едитор установлен и находится на указаном пути,скорее всего для macOS не нужно будет менять путь, установка на них происходит обычно в одно и тоже место

# UNITY_LOCATION_MACOS : "2021.3.26f1 , installed at /Applications/Unity/Hub/Editor/2021.3.26f1/Unity.app/Contents/MacOS/Unity" # Хард код строка, алтернатива Settings->CI/CD->Variables

UNITY_LOCATION_MACOS: $UNITY_LOCATION_MACOS

# Альтернатива

# Хард кор версия доступна для разработчиков с правами developers

# Альтернатива доступна разработчикам с правами maintainers

ID: $Project_ID
BUNDLEANDROID: $Bundle_Version_Code
BUILDIOS: $Build_iOS
TARGETVERSION: $Target_SDK

#Сервер для сбора кеша, изменение в данной переменной не требуются
UNITY_CACHE_SERVER: "localhost:8126"

PACKAGE_REGISTRY_URL: "https://$DEPLOY_TOKEN_USERNAME:$DEPLOY_TOKEN_PASS@gitlab.b5a0.space/api/v4/projects/${CI_PROJECT_ID}/packages/generic/releases/${PACKAGE_VERSION}"

# Используйте cache, чтобы указать список файлов и каталогов для кэширования между заданиями. Вы можете использовать только пути, находящиеся в локальной рабочей копии. (https://docs.gitlab.com/ee/ci/yaml/#cache)

cache:

# Параметр job, который указывает путь для создания файлов в процесе выполнение job

paths: - node_modules/

package-registry:macos-android:
stage: Build
when: manual
tags: - macOS
rules: - if: $CI_COMMIT_TAG
  cache:
    key: windows
    paths:
      - Library/
  variables:
    BUILD_TARGET: "Android"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
  script:
    - npm i
    - npx gulp buildWindows
    - curl --upload-file ./$OUTPUT*DIR/*$PACKAGE_VERSION/Demo.apk "${PACKAGE_REGISTRY_URL}/macos-Android-Build"

package-registry:windows-android:
stage: Build
when: manual
tags: - windows
rules: - if: $CI_COMMIT_TAG
  cache:
    key: windows
    paths:
      - Library/
  variables:
    BUILD_TARGET: "Android"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
  script:
    - npm i
    - npx gulp buildWindows
    - $pair = "$($DEPLOY_TOKEN_USERNAME):$($DEPLOY_TOKEN_PASS)"
    - $encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair)) - $basicAuthValue = "Basic $encodedCreds"
    - |
      $Headers = @{
        Authorization = $basicAuthValue
      }
    - Invoke-WebRequest -Uri $PACKAGE_REGISTRY_URL/windows-Android-Build -Headers $Headers -Method Put -InFile ./$OUTPUT*DIR/*$PACKAGE_VERSION/Demo.apk

package-registry:simulator-debug-ios:
stage: Build
when: manual
tags: - macOS
rules: - if: $CI_COMMIT_TAG
  cache:
    key: macOS
    paths:
      - Library/
  variables:
    BUILD_TARGET: "iOS"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
    ID: $Project_ID
  script:
    - npm i
    - npx gulp buildWindows
    - cd ./$OUTPUT*DIR/*$BUILDIOS
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug -allowProvisioningUpdates -destination 'generic/platform=iOS Simulator'
    - xcodebuild archive -quiet -archivePath build/UnityGame -allowProvisioningUpdates -configuration Debug DEVELOPMENT_TEAM=Y6TZPN9V5V -scheme Unity-iPhone CODE_SIGN_STYLE=Automatic -destination 'generic/platform=iOS Simulator'
    - zip -vr "UnityGame.zip" ./build/UnityGame.xcarchive
    - curl --upload-file ./UnityGame.zip "${PACKAGE_REGISTRY_URL}/ios-simulator-debug-app"

package-registry:simulator-release-ios:
stage: Build
when: manual
tags: - macOS
rules: - if: $CI_COMMIT_TAG
  cache:
    key: macOS
    paths:
      - Library/
  variables:
    BUILD_TARGET: "iOS"
    BACKEND_BASE_URL: "https://develop.mydomain.com/mygame"
    ID: $Project_ID
  script:
    - npm i
    - npx gulp buildWindows
    - cd ./$OUTPUT*DIR/*$BUILDIOS
    - xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release -allowProvisioningUpdates -destination 'generic/platform=iOS Simulator'
    - xcodebuild archive -quiet -archivePath build/UnityGame -allowProvisioningUpdates -configuration Debug DEVELOPMENT_TEAM=Y6TZPN9V5V -scheme Unity-iPhone CODE_SIGN_STYLE=Automatic -destination 'generic/platform=iOS Simulator'
    - zip -vr "UnityGame.zip" ./build/UnityGame.xcarchive
    - curl --upload-file ./UnityGame.zip "${PACKAGE_REGISTRY_URL}/ios-simulator-release-app"

Release:
stage: Release
when: manual
tags: - macos
rules: - if: $CI_COMMIT_TAG
script: - echo "Creating script..."
release:
name: "Release $CI_COMMIT_TAG"
tag_name: $CI_COMMIT_TAG
ref: $CI_COMMIT_TAG
description: "Release desription"
assets:
links: - name: windows-Android-Build
url: $PACKAGE_REGISTRY_URL/windows-Android-Build
link_type: package - name: macos-adroid-Build
url: $PACKAGE_REGISTRY_URL/macos-Android-Build
link_type: package - name: ios-simulator-Debug-app
url: $PACKAGE_REGISTRY_URL/ios-simulator-debug-app
link_type: package - name: ios-simulator-release-app
url: $PACKAGE_REGISTRY_URL/ios-simulator-release-app
link_type: package
```
