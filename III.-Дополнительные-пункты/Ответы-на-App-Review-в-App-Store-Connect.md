# App Store Connect Review

Для ответа на запрос App Review по Guideline 2.1 - Information Needed в App Store Connect, следует предоставить необходимую информацию, которую запросили из команды App Review.

1. **Оценка запроса**: Внимательно прочитайте запрос от команды App Review.

2. **Подготовка ответов**: Предоставляем подробные и ясные ответы на вопросы с требуемой информацией и документацией.

3. **Сбор доказательств**:  Предоставляем необходимые доказательства по запросу, такие как скриншоты, видеозаписи, ссылки и т. д.

4. **Форматирование и структура**: Оформляем ответы в читаемой и структурированной форме с использованием пунктов или нумерации.

5. **Своевременная отправка**:  Отправляем ответ в установленные сроки для ускорения рассмотрения.

6. **Подготовка к обсуждению**: Готовы предоставить дополнительную информацию или уточнения по запросу.

7. **Следим за обновлениями**: Регулярно проверяем почту в App Store Connect и форумы разработчиков Apple для обновлений и дополнительных запросов.

# Пример ответа, отвечать нужно только на английском языке:

<table>
<tr>

    Hello,

    Thank you for your message. We appreciate your attention to our app and are happy to provide the requested information regarding Guideline 2.1 - Information Needed.

    1. **Third-party analytics**: Yes, our app includes third-party analytics provided by [Analytics Provider Name]. This analytics tool collects anonymized data such as user interactions, device information, and app usage patterns. This data is used solely for the purpose of improving the app's performance and user experience.

    2. **Third-party advertising**: No, our app does not include third-party advertising.

    3. **Data sharing with third parties**: No, we do not share user data with any third parties for any purposes. All user data collected by our app is kept strictly confidential and is not shared, sold, or distributed.

    4. **User or device data**: Our app collects minimal user data for the sole purpose of providing core app functionality. This data includes [describe the data collected, if applicable]. None of this data is used for third-party analytics or advertising. It is stored securely on our servers and is not shared with any third parties.

    We hope this information is sufficient to proceed with the review of our app. Please let us know if you require any additional details or if there are any specific concerns you would like us to address.

    Thank you for your prompt attention to this matter.

    Best regards,

    [Имя уточнять у ТЛ]
    [Название вашей компании уточнять у ТЛ]

</tr>
</table>

# Пример ответа на русском только для понимания сути, его при ответе не используем:

<table>
<tr>

    Здравствуйте,

    Спасибо за ваше сообщение. Мы благодарим за внимание к нашему приложению и готовы предоставить запрошенную информацию в соответствии с рекомендацией 2.1 - Необходимая информация.

    1. **Аналитика от третьих лиц**: Да, наше приложение включает в себя аналитический инструмент от [Название аналитического провайдера]. Этот инструмент собирает анонимизированные данные, такие как взаимодействие пользователей, информацию об устройстве и паттерны использования приложения. Эти данные используются исключительно для улучшения производительности приложения и опыта пользователей.

    2. **Реклама от третьих лиц**: Нет, в нашем приложении нет рекламы от третьих лиц.

    3. **Обмен данных с третьими сторонами**: Нет, мы не передаем данные пользователей третьим сторонам ни для каких целей. Все данные пользователей, собранные нашим приложением, строго конфиденциальны и не передаются, не продается и не распространяется.

    4. **Сбор данных о пользователях или устройствах**: Наше приложение собирает минимальные данные о пользователях исключительно для обеспечения основной функциональности приложения. Эти данные включают в себя [опишите данные, если они собираются]. Ни один из этих данных не используется для сторонней аналитики или рекламы. Они хранятся надежно на наших серверах и не передаются третьим сторонам.

    Мы надеемся, что предоставленная информация достаточна для продолжения рассмотрения нашего приложения. Пожалуйста, дайте нам знать, если вам требуется дополнительная информация или если у вас есть какие-либо конкретные вопросы.

    Спасибо за оперативное внимание к данному вопросу.

    С наилучшими пожеланиями,

    [Имя уточнять у ТЛ]
    [Название вашей компании уточнять у ТЛ]

</tr>
</table>