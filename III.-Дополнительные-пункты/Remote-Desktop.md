# Правила использования корпоративных ПК (macOS/Windows) через RustDesk/AnyDesk

1.  Создавать проекты, инициировать, клонировать можно только в [Директорию](/III.-%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BF%D1%83%D0%BD%D0%BA%D1%82%D1%8B//1.-%D0%93%D0%BB%D0%BE%D1%81%D1%81%D0%B0%D1%80%D0%B8%D0%B9#%D0%B4%D0%B8%D1%80%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%B8%D1%8F) **/Documents**:

    ```
    cd ~/Documents
    ```
    Эта команда позволяет перейти в директорию **/Documents** через терминал.
2. Запрещается сохранять [учетные записи](/III.-Дополнительные-пункты/Глоссарий#залогинится) (в том числе и корпоративные) на удалённых ПК (1. Браузер, 2. Терминал и тд.). Запрещается [логиниться](/III.-Дополнительные-пункты/Глоссарий#залогинится) под своими личными данными. 
    1. Использовать приватный режим веб-браузера, такой как режим инкогнито в Google Chrome или приватный режим в Safari для предотвращения сохранения данных о сеансе, таких как история посещений и кэшированные данные.
    2. Если в терминале клонировать репозиторий без логина и пароля, данные будут сохраняться в [Keychain](/III.-Дополнительные-пункты/Глоссарий#keychain) удаленного ПК, что недопустимо:
    
          ```
          git clone https://@192.168.1.7:3014/dev0/repository.git
          ```

3. Если нужно воспользоваться программой, для работы с которой необходима авторизация (пример: [GitHub Desktop](/III.-Дополнительные-пункты/Глоссарий#залогинится)), аккаунт можно создать через [Temp-Mail](https://temp-mail.org) и отправить [кредентиалы](/III.-Дополнительные-пункты/Глоссарий#залогинится) [ТЛ](/III.-Дополнительные-пункты/Глоссарий#тл).
4. Если возникает ошибка при клонировании репозитория через терминал ![ClonningErrorInTerminal](/Images//RemoteDesktop/ClonningErrorInTerminal.jpeg){:width="280px" height="340px"} 

   Есть такие варианты решения:
   1. Нужно добавить к запросу логин и пароль при клонировании: `login:password`:

       - Пример клонирования последнего коммита, ветки `mybranch` , имя репозитория `repository` , `login:password` от [Репозитория](/III.-%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BF%D1%83%D0%BD%D0%BA%D1%82%D1%8B//1.-%D0%93%D0%BB%D0%BE%D1%81%D1%81%D0%B0%D1%80%D0%B8%D0%B9#git) :

          ```
          git clone --depth 1 -b mybranch --single-branch 
          https://login:password@192.168.1.7:3014/dev0/repository.git
          ```

       - Для клонирования полного репозитория `url` может быть (gitlab.domainName) так и в формате IP 192.168.1.7:3014 как в примере выше:

          ```
          git clone https://login:password@url/dev0/repository.git
          ```
       
      
    2. Удалить старые учетные записи к репозиторию, сохраненные в macOS [Keychain](/III.-Дополнительные-пункты/Глоссарий#keychain):
          ```
          git credential-osxkeychain erase
          host=gitlab.b5a0.space
          protocol=https
          ```
5. Для передачи файлов между удалённым компьютером и локальным нажимаем кнопку со значком молнии Actions =\> File Manager (Не использовать этот способ для добавления проекта на MacOS). **Не использовать при передаче файлов облачные хранилища, по типу: Google Drive, Dropbox, Microsoft OneDrive или другие**.
5.  Для изменения разрешения экрана удалённого рабочего стола используется утилита displayplacer или стандартные средства MacOS (System Preferences =\> Displays)
6.  Устанавливать приложения, менять данные на подобии обоев рабочего стола и прочее можно только с разрешения [ТЛ](/III.-%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BF%D1%83%D0%BD%D0%BA%D1%82%D1%8B//1.-%D0%93%D0%BB%D0%BE%D1%81%D1%81%D0%B0%D1%80%D0%B8%D0%B9#%D1%82%D0%BB)
7. Для управления Android устройством подключённым к ПК, используем утилиту [Scrcpy](https://github.com/Genymobile/scrcpy). [см.Инструкция по использованию Scrcpy](/%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%BD%D0%B8%D1%86%D1%8B//%D0%98%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-Scrcpy)
8. По окончании рабочего дня:
   1. Компьютер не выключать
   2. Должно быть закрыты все программы кроме [Activity Monitor](/Images//Macos/Activity_Monitor_MacOS.png) и [Ремоут](/III.-%D0%94%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BF%D1%83%D0%BD%D0%BA%D1%82%D1%8B//1.-%D0%93%D0%BB%D0%BE%D1%81%D1%81%D0%B0%D1%80%D0%B8%D0%B9#%D1%80%D0%B5%D0%BC%D0%BE%D1%83%D1%82) (рядом с не закрытыми программами в [панели Dock](/Images//Macos/Dock_panel.png) присутствует точка/маркер). [![](///Images//Macos/Doc.png)](///Images//Macos/DocFull.png)
   3. Всё созданное за рабочий день, что не пригодится завтра, удаляем(скриншоты, ненужные скачанные файлы и тд.). Корзину очищаем.
   4. Проверяем через [Activity Monitor](/Images//Macos/Activity_Monitor_MacOS.png) что в вкладке CPU все ресурсоёмкие процессы* закрыты.
   5. Присылаем ТЛ скриншот полного рабочего стола удалённого ПК, c видимым [Activity Monitor](/Images//Macos/Activity_Monitor_MacOS.png) и [панелью Dock](/Images//Macos/Dock_panel.png) в левой части экрана.
9. Если действия не совершаются выше 50 минут, ресурсоёмкие процессы* должны быть завершены.
10. Если в процессе работы на удаленном рабочем столе появляется окно с информацией о входе на другое устройство
   
   ![Your Apple ID is being used to sign in to a new device](///Images//RemoteDesktop/Window_for_signin_confirmation.jpg)
   
   Тогда нужно сделать следующее:
   1. Нажать кнопку "Allow".
   2. В открывшемся окне будет 6-ти значительный код. Его необходимо отправить ТЛ.
   
   ![Verification code](///Images//RemoteDesktop/Window_for_signin_verification_code.jpg)
   
*ресурсоёмкие процессы - непрерывное использование [CPU](/Images//Macos/Activity_Monitor_MacOS.png) от 5% .

11. Для копирование текстовой информации использовать комбинацию клавиш 'windows + c' - копирование, 'windows + v'  - вставка. Если на вашем локальном компьютере уже назначены действия на эти комбинации клавиш и они конфликтуют с копированием/вставкой, то необходимо освободить их в настройках 'Keyboard Layout' вашей операционной системы.  

# Дополнительная информация

Для использования Mac есть следующие пары логин/пароль:

1. RustDesk(Anydesk) - пара которая используется для удалённого подключения к Mac;
2. MacOS - пара от операционной системы MacOS, например для подтверждения действий в терминале, требующих доступ администратора;
3. AppleID - пара от AppleID используется для обновления/скачивания приложений в AppStore & доступу к AppStore connect.

# Board View

Чтобы просмотреть соотвествие клавиш  нужно нажать Show Keyboard View 

[<img src="///Images//RemoteDesktop/ShowKeyboardView.png" width="500">](///Images//RemoteDesktop/ShowKeyboardView.png)

Далее отобразиться панель с клавиатурой

[<img src="///Images//RemoteDesktop/KeyBoard.png" width="500">](///Images//RemoteDesktop/KeyBoard.png)

# Как закрыть неотвечающее приложение с помощью Activity Monitor

1. Открыть програму Activity Monitor.
2. В программе Activity Monitor выбрать программу, которую нужно закрыть.
3. Нажать символ Х в верхней части окна.

[<img src="///Images//RemoteDesktop/ActivityStopProcess.png" width="500">](///Images//RemoteDesktop/ActivityStopProcess.png)

4. На экране подтверждения нажать кнопку "Quit".

[<img src="///Images//RemoteDesktop/ActivityQuit.png" width="500">](///Images//RemoteDesktop/ActivityQuit.png)

# Как провести комбинацию клавиш

Произведение комбинации клавиш на примере включения отображения скрытых файлов в директории. Выполнение комбинаци клавиш `CMD` + `Shift` + `.`

1. Открыть панель [Board View](#board-view)
2. Зажать кнопку `CMD` пока она не станет обводится красной полосой.
3. Нажать кнопки `Shift` и `.`.

[<img src="///Images//RemoteDesktop/CMDShiftDot.png" width="500">](///Images//RemoteDesktop/CMDShiftDot.png)