# Содержание

- [Зборка через Unity](#зборка-через-unity)
  - [Зборка і запуск Unity android застосунків](#зборка-і-запуск-unity-android-застосунків)
  - [Зборка Unity iOS застосунків](#зборка-unity-ios-застосунків)
  - [Возможные ошибки при сборке в IDE Unity](#возможные-ошибки-при-сборке-в-ide-unity)
    - [Cannot create directory](#cannot-create-directory)
    - [Unable to sign the application please provide passwords](#unable-to-sign-the-application-please-provide-passwords)
    - [Target Android SDK not installed](#target-android-sdk-not-installed)
- [Сборка и запуск приложений через Android Studio и Xcode](#сборка-и-запуск-приложений-через-android-studio-и-xcode)
  - [Flutter](#flutter)
  - [Сборка iOS приложения через Xcode](#сборка-ios-приложения-через-xcode)
    - [Сборка iOS приложения через Xcode для релиза](#сборка-ios-приложения-через-xcode-для-релиза)
  - [Исправление проблем с xCode](#исправление-проблем-с-xcode)
    - [Удаление advertisingIdentifier selectors и ASIdentifierManager](#удаление-advertisingidentifier-selectors-и-asidentifiermanager)
    - [Undefined symbols](#undefined-symbols)
  - [Сборка и запуск Android приложения через Android Studio](#сборка-и-запуск-android-приложения-через-android-studio)
    - [Сборка и запуск Android приложения через Android Studio для разработки](#сборка-и-запуск-android-приложения-через-android-studio-для-разработки)
  - [Исправление проблем с Android Studio](#исправление-проблем-с-android-studio)
    - [Installation did not succeed. The application could not be installed](#installation-did-not-succeed-the-application-could-not-be-installed)
  - [Тестирование react native приложения на Android](#тестирование-react-native-приложения-на-android)
    - [Возможные проблемы при тестировании](#возможные-проблемы-при-тестировании)
- [Зборка через VSCode](#зборка-через-vscode)
  - [React Native зборка через VSCode](#react-native-зборка-через-vscode)

# Зборка через Unity
Есть два вида сборки: дебаг и [релизная](///Дополнительные-странницы/Загрузка-приложения-в-маркет#как-зделать-релизный-билд).
По умолчанию стоит релизная сборка. 
[Инструкция по созданию релизной сборки](///Дополнительные-странницы/Загрузка-приложения-в-маркет#как-зделать-релизный-билд)




## Зборка і запуск Unity android застосунків

1. Використовувати модулі встановлені з Unity. Для цього перейти в `Preferences > External Tools`. Поставити галочки в `Android` секції біля `JDK`, `SDK`, `NDK`

[<img src="///Images//IDE/unity-use-auto-modules.png" width="500">](///Images//IDE/unity-use-auto-modules.png)

2. Перейти в `Build Settings`. Вибрати платформу `Android`. Може знадобитися натиснути кнопку `Switch Platfrom` після чого з'являться кнопки `Build` для зборки і `Build and Run` для зборки і запуску на симуляторі

[<img src="///Images//IDE/unity-android-build.png" width="500">](///Images//IDE/unity-android-build.png)

## Зборка Unity iOS застосунків

1. Налаштувати Unity для автоматичного підпису iOS застосунків. Для цього: зайти в `Project Settings > Player > iOS`. В секції `Identification` поставити галочку напроти `Automatically Sign` і вказати `Signing Team ID`. Додатково вказати `Target SDK` - `Device SDK` якщо потрібна зборка для реального девайсу або `Simulator SDK` якщо потрібна зборка для симулятора

[<img src="///Images//IDE/unity-auto-sign-ios.png" width="500">](///Images//IDE/unity-auto-sign-ios.png)

[<img src="///Images//IDE/unity-ios-target.png" width="500">](///Images//IDE/unity-ios-target.png)

2. Якщо в проекті встановлений [External Dependency Manager](https://github.com/googlesamples/unity-jar-resolver) - зайти в "Assets"->"External Dependency Manager"->"IOS Resolver" примінити "install Cocoapods"

3. Перейти в `Build Settings`. Вибрати платформу `iOS`. Може знадобитися натиснути кнопку `Switch Platfrom` після чого з'являться кнопки `Build` для зборки або `Build and Run` для зборки і запуску на симуляторі

[<img src="///Images//IDE/unity-ios-build.png" width="500">](///Images//IDE/unity-ios-build.png)

4. Якщо в проекті встановлений [External Dependency Manager](https://github.com/googlesamples/unity-jar-resolver):

   - Открыть проект в терминале(`ПКМ по папке с проектом -> New terminal at folder`)

   - Если Mac на процессорах Intel: применить команду:
     ```
     pod install --repo-update
     ```
     В ином случае смотреть подходящею по [инструкции](https://www.youtube.com/watch?v=zdv9qE4j-VU)
   - Если во время выполнения появились ошибки по типу `Errno::EACCES - Permission denied`, тогда нужно перейти в папку с кешем: `/Users/{myusername}/Library/Caches/`, найти, удалить и заново создать папку `CocoaPods`, после чего выполнить команду заново

## Возможные ошибки при сборке в IDE Unity

### Cannot create directory

Ошибка `"Cannot create directory <User>/.android"` где `<User>` путь к папке пользователя.

[<img src="///Images//IDE/Error_Cannot_Create_Directory.png" width="500">](///Images//IDE/Error_Cannot_Create_Directory.png)

Ошибка появляется если по этому пути невозможно создать папку `.android`. Это может произойти если в директории существует файл с таким же именем.

Чтоб решить эту ошибку нужно открыть отображение скрытых файлов, в директории `<User>/`. Пример на MacOS провести [комбинацию клавиш](///III.-Дополнительные-пункты//Remote-Desktop#как-провести-комбинацию-клавиш) `CMD` + `Shift` + `.`. 

Далее удалить/перенести/переименовать файл `.android`.

Результат: 

[<img src="///Images//IDE/Successful_result.png" width="500">](///Images//IDE/Successful_result.png)

### Unable to sign the application please provide passwords

В випадку якщо при натисканні `Build` або `Build and Run` виведеться помилка : `Unable to sign the application please provide passwords ` треба ввести пароль який вказаний в `Readme` проекту.

[<img src="///Images//IDE/unity-android-build-passwords.png" width="500">](///Images//IDE/unity-android-build-passwords.png)

### Target Android SDK not installed

Якщо виведеться помилка версії target SDK то треба скачати потрібну версію target SDK.
Приклад помилки:

[<img src="///Images//IDE/Target-sdk-eror.png" width="500">](///Images//IDE/Target-sdk-eror.png)

Для того щоб завантажити потрібну версію target SDK потрібно ввести цю команду в термінал: `"D:\workspace\tools\2021.3.26f1\Editor\Data\PlaybackEngines\AndroidPlayer\SDK\tools\bin\sdkmanager.bat" "platforms;android-28"` при цьому вказати свій власний шлях та потрібну версію.

# Сборка и запуск приложений через Android Studio и Xcode
  Первая 
[сборка](///III.-Дополнительные-пункты//Глоссарий#build) приложения всегда выполняется через Android Studio(для android) или Xcode(для iOS).

## Flutter

- Для первичной [сборки](///III.-Дополнительные-пункты//Глоссарий#build) открываем в терминале корень проекта и выполняем команду [сборки](///III.-Дополнительные-пункты//Глоссарий#build) для ios средствами flutter “flutter build ios”.
- Далее открываем iOS проект в <b>XCode</b> зайдя в под-папку ios проекта flutter. Важно открывать именно рабочую область **Runner.xcworkspace**, а не **Runner.xcodeproj**.

## Сборка iOS приложения через Xcode

iOS приложение можно запускать на [pеальном устройстве](///Дополнительные-странницы//Как-запускать-IPA#запуск-ios-приложения-через-реальное-устройство), [симуляторе](///Дополнительные-странницы//Как-запускать-IPA#запуск-ios-приложения-через-симулятор), [MacBook с процессом из серии Apple Silicon](///Дополнительные-странницы//Как-запускать-IPA#запуск-ios-приложения-через-мacos-с-apple-silicon).

Некоторые функции, такие как Push-уведомления, не работают на симуляторе, поэтому для тестирования нужно использовать реальное устройство.

### Сборка iOS приложения через Xcode для релиза

1. Открыть проект в Xcode.
2. Выбрать главный раннер во вкладке проектов.

[<img src="///Images//BuildAndRun/select-the-main-runner_censored.jpg" width = "500">](///Images//BuildAndRun/select-the-main-runner_censored.jpg)

3. Выбрать таргет.

[<img src="///Images//BuildAndRun/select-target_censored.jpg" width = "500">](///Images//BuildAndRun/select-target_censored.jpg)

4. Нажать на вкладку <b>General</b>.

[<img src="///Images//BuildAndRun/select-general_censored.jpg" width = "500">](///Images//BuildAndRun/select-general_censored.jpg)

5. Заполнить <b>Version</b>, <b>Display Name</b>, <b>Bundle Identifier</b>, <b>Build</b>. Важно, чтобы версия была больше той, которая уже опубликована, иначе проект не опубликуется.

[<img src="///Images//BuildAndRun/select-general-config_censored.jpg" width = "500">](///Images//BuildAndRun/select-general-config_censored.jpg)

6. Нажать на вкладке <b>Signing & Capabilities</b>.

[<img src="///Images//BuildAndRun/select-signing-and-capabilities_censored.jpg" width = "500">](///Images//BuildAndRun/select-signing-and-capabilities_censored.jpg)

7. Заполнить поле <b>Team</b>.

[<img src="///Images//BuildAndRun/select-team_censored.jpg" width = "500">](///Images//BuildAndRun/select-team_censored.jpg)

8. Выбрать устройство, для которого создаётся сборка:

[<img src="///Images//BuildAndRun/select-a-real-device_censored.jpg" width = "500">](///Images//BuildAndRun/select-a-real-device_censored.jpg)

- Если нужно протестировать приложение перед релизом, то выбрать устройство, на котором будет тестироваться приложение.
- Если нужно собрать приложение для релиза, то выбрать <b>Any iOS device (arm64)</b>.

9. Нажать в верхнем меню <b>Product > Build</b>.

[<img src="///Images//BuildAndRun/product-build_censored.jpg" width = "500">](///Images//BuildAndRun/product-build_censored.jpg)

10. Когда [билд](///III.-Дополнительные-пункты//Глоссарий#build) завершился, то нажать в верхнем меню <b>Product > Archives</b>, чтобы создать IPA-файл. Если во время архивации попросят ввести пароль, то ввести пароль от MacBook.

[<img src="///Images//BuildAndRun/product-archive_censored.jpg" width = "500">](///Images//BuildAndRun/product-archive_censored.jpg)

11. После окончания архивация появится окно органайзера в выбранной вкладкой <b>Archive</b>. Если окно органайзера не появилось, то открыть это окно, нажав <b>Window > Organizer</b>, а затем выбрать вкладку <b>Archives</b>.

[<img src="///Images//BuildAndRun/window-organized-archives_censored.jpg" width = "500">](///Images//BuildAndRun/window-organized-archives_censored.jpg)

12. Выбрать созданный архив. Нужный архив можно найти по времени создания и версии.

[<img src="///Images//BuildAndRun/select-archive_censored.jpg" width = "500">](///Images//BuildAndRun/select-archive_censored.jpg)

13. Нажать <b>Validate App</b>, чтобы провести минимальную автоматическую валидацию приложения. Нужно разрешить ошибки или предупреждения после валидации.

[<img src="///Images//BuildAndRun/validate-app_censored.jpg" width = "500">](///Images//BuildAndRun/validate-app_censored.jpg)

14. Нажать <b>Distribute App</b>.

[<img src="///Images//BuildAndRun/press-distribute_censored.jpg" width = "500">](///Images//BuildAndRun/press-distribute_censored.jpg)

15. Если нужно отправить приложение в App Store, то в окне <b>Select method of distribution</b> выбрать <b>Ad Store Connect</b>.

    [<img src="///Images//BuildAndRun/distribute-to-app-store-connect_censored.jpg" width = "500">](///Images//BuildAndRun/distribute-to-app-store-connect_censored.jpg)

- В окне <b>Select Destination</b> ничего не менять и нажать <b>Next</b>

  [<img src="///Images//BuildAndRun/app-store-connect-destination_censored.jpg" width = "500">](///Images//BuildAndRun/app-store-connect-destination_censored.jpg)

- В окне <b>App Store Connect distribution options</b> ничего не менять и нажать <b>Next</b>.

  [<img src="///Images//BuildAndRun/app-store-connect-distribution-options_censored.jpg" width = "500">](///Images//BuildAndRun/app-store-connect-distribution-options_censored.jpg)

- В окне <b>Re-sign</b> ничего не менять и нажать кнопку <b>Next</b>.

  [<img src="///Images//BuildAndRun/resign_censored.jpg" width = "500">](///Images//BuildAndRun/resign_censored.jpg)

- В окне <b>Review</b> нужно нажать кнопку <b>Upload</b>.

  [<img src="///Images//BuildAndRun/review-app-store-connect_censored.jpg" width = "500">](///Images//BuildAndRun/review-app-store-connect_censored.jpg)

## Исправление проблем с xCode

### Удаление advertisingIdentifier selectors и ASIdentifierManager

1. Найдите файл `DeviceSettings.mm` в своем проекте Xcode (вы должны найти его в разделе `Classes/Unity/`)

2. Внесите туда следующие изменения:

   2.1. Удалите следующие функции:

```
        static id QueryASIdentifierManager()
         {
         <..>
         }

```

2.2. Удалите следующие объявления переменных:

```
      static NSString*    _ADID               = nil;
      static bool         _AdTrackingEnabled  = false;
```

2.3. Измените реализации следующих функций (замените представленными ниже):

```
      extern "C" const char*  UnityAdIdentifier()
         {
         return NULL;
         }

         extern "C" bool         UnityAdTrackingEnabled()
         {
         return false;
         }

```

**Исходная ссылка решение этой проблемы:** `https://forum.unity.com/threads/ios-advertising-identifier-rejection-faq.226187/`

### Undefined symbols

Удалить выделенный на скриншоте ниже файл из списка "Framework and Libraries"

[<img src="///Images//BuildAndRun/Framework.png" width = "500">](///Images//BuildAndRun/Framework.png)

### FBReactNativeSpec Command PhaseScriptExecution failed with a nonzero exit code при сборке

Нужно попробовать:

1. Ввести в терминал следующую команду, чтобы получить путь к используемой версии `node`:

```
which node
```

Например: `/Users/teal/.nvm/versions/node/v18.16.1/bin/node`

2. Скопировать этот путь.

3. В Xcode открыть таргет приложения, выбрать вкладку "Build Phases" и открыть "Bundle React Native code and images".

4. Добавить в качестве значения `NODE_BINARY` скопированный путь `node` и экспортировать его.

Например:

```
export NODE_BINARY=/Users/teal/.nvm/versions/node/v18.16.1/bin/node
```

[<img src="///Images//BuildAndRun/export-node-binary.jpg" width = "500">](///Images//BuildAndRun/export-node-binary.jpg)

## Сборка и запуск Android приложения через Android Studio

### Сборка и запуск Android приложения через Android Studio для разработки

1. Открыть проект в <b>Android Studio</b>.

2. Подождать, пока проект загрузится.

[<img src="///Images//BuildAndRun/adnroid-wait-util-project-is-loaded_censored.jpg" width = "500">](///Images//BuildAndRun/adnroid-wait-util-project-is-loaded_censored.jpg)

3. Вибрати конфігурацію зборки. Натиснути `Build > Select Build Variant`. Напроти `:app` вибрати `release` з випадаючого списка

[<img src="///Images/IDE/android-studio-release-config.png" width="500">](/Images/IDE/android-studio-release-config.jpeg)

4. Выбрать устройство, на котором будет запускаться приложение.

[<img src="///Images//BuildAndRun/android-select-real-device-or-simulator_censored.jpg" width = "500">](///Images//BuildAndRun/android-select-real-device-or-simulator_censored.jpg)

5. Нажать на кнопку <b>Run</b>.

[<img src="///Images//BuildAndRun/press-the-run-button_censored.jpg" width = "500">](///Images//BuildAndRun/press-the-run-button_censored.jpg)

## Исправление проблем с Android Studio

### Installation did not succeed. The application could not be installed

Чтобы исправить данную проблему, можно попробовать очистить кеш **File -> Invalidate Caches -> Restart**

[<img src="///Images//BuildAndRun/android-invalidate-cache.png" width = "500">](///Images//BuildAndRun/android-invalidate-cache.png)

Если проблема осталась, можно удалить приложение с эмулятора Android Studio и переустановить его. Для этого необходимо перейти в **Настройки эмулятора Android Studio --> Приложения --> Найти в списке искомое приложение, кликнуть на него --> нажать кнопку Удалить**, после этого зайти в **Файлы (Проводник) --> Скачанные файлы --> найти необходимый [билд](///III.-Дополнительные-пункты//Глоссарий#build)** и установить его на эмулятор

## Тестирование react native приложения на Android

1. Склонировать репозиторий с **GitLab**:

```
git clone
```

2. Перейти в директорию проекта и запустить команду:

```
npm install
```

или

```
yarn install
```

3. Запустить команду:

```
npx react-native run-android
```

#### После этого приложение должно собраться и запуститься либо на ранее созданом в **Android Studio** эмуляторе, либо на подключенном через USB физическом устройстве.

### Возможные проблемы при тестировании

1. Если при повторной сборке и запуска приложения возникают ошибки в самом приложении, их можно решить одним из способов:

- Перезагрузка физического устройства
- Очистка кэша приложения.

2. Приложение может не собираться из-за устаревшей версии _Java SDK_. На Mac устройстве это можно сделать с помощью команды:

```
brew install
```

#### Установка нужной версии **Java** и правильная настройка переменных среды в системе должны исправить проблему.

# Зборка через VSCode

## React Native зборка через VSCode

1. [Cтворити релізний keystore файл](https://reactnative.dev/docs/signed-apk-android#generating-an-upload-key) для підпису релізної android [сборки](///III.-Дополнительные-пункты//Глоссарий#build). Під час створення keystore файлу, знадобиться згенерувати пароль, вказати аліас до нього. Зберегти пароль, аліас і створений keystore файл.

2. Створити в корені проекту директорію `.vscode`. Створити в новії директорії директорію `scripts`.

3. Додати в директорію `.vscode` `tasks.json` файл:

```
{
  // See https://go.microsoft.com/fwlink/?LinkId=733558
  // for the documentation about the tasks.json format
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Android debug build",
      "type": "shell",
      "command": "./.vscode/scripts/android/android-debug-build.sh",
      "windows": {
        "command": "powershell -File ./.vscode/scripts/android/android-debug-build.ps1"
      },
      "group": "build",
      "presentation": {
        "reveal": "always",
        "panel": "new"
      }
    },
    {
      "label": "Android release build",
      "type": "shell",
      "command": "./.vscode/scripts/android/android-release-build.sh",
      "windows": {
        "command": "powershell -File ./.vscode/scripts/android/android-release-build.ps1"
      },
      "group": "build",
      "presentation": {
        "reveal": "always",
        "panel": "new"
      }
    },
    {
      "label": "IOS simulator debug build",
      "type": "shell",
      "command": "./.vscode/scripts/ios/simulator-debug-build.sh",
      "group": "build",
      "presentation": {
        "reveal": "always",
        "panel": "new"
      }
    },
    {
      "label": "IOS simulator release build",
      "type": "shell",
      "command": "./.vscode/scripts/ios/simulator-release-build.sh",
      "group": "build",
      "presentation": {
        "reveal": "always",
        "panel": "new"
      }
    }
  ]
}
```

4.  Створити в директорії `scripts` директорії `android` і `ios`. В директорію `android` додати наступні файли:

    1.  `android-debug-build.ps1`:

        ```
        echo 'starting android debug build...'
        cd android
        ./gradlew assembleDebug
        ```

    2.  `android-debug-build.sh`:

        ```
        #!/bin/sh
        echo 'starting android debug build...'
        cd android
        chmod +x ./gradlew
        ./gradlew assembleDebug
        ```

    3.  `android-release-build.ps1`:

        ```
        echo 'Adding env variables..'
        $ANDROID_KEY_ALIAS = 'my-key-alias'
        $ANDROID_KEY_STOREFILE = './my-upload-key.keystore'
        $ANDROID_KEYSTORE_PASSWORD = <your_keystore_password>

        echo 'Added env variables. Starting release bundling...'
        cd android
        ./gradlew :app:bundleRelease
        ```

        Пояснення змінних:

        1. ANDROID_KEY_STOREFILE - шлях до релізного keystore файлу
        2. ANDROID_KEY_ALIAS - аліас keystore файлу
        3. ANDROID_KEYSTORE_PASSWORD - вказувати пароль до keystore файлу

        [Опис команд RN android зборки](///Дополнительные-странницы///Опис-команд-RN-android-зборки)

    4.  `android-release-build.sh`:

        ```
        #!/bin/sh
        echo 'starting android release build...'

        export ANDROID_KEYSTORE_ALIAS='release-test-app'
        export ANDROID_KEYSTORE_FILE='./release-test-app.keystore'
        export ANDROID_KEYSTORE_PASSWORD=<your_keystore_password>
        cd android
        chmod +x ./gradlew
        ./gradlew :app:bundleRelease
        ```

        Пояснення змінних:

        1. ANDROID_KEY_STOREFILE - шлях до релізного keystore файлу
        2. ANDROID_KEY_ALIAS - аліас keystore файлу
        3. ANDROID_KEYSTORE_PASSWORD - вказувати пароль до keystore файлу

        [Опис команд RN android зборки](///Дополнительные-странницы///Опис-команд-RN-android-зборки)

    Створити `ios` В директорію `ios` додати наступні файли:

    1.  `simulator-debug-build.sh`:

        ```
        #!/bin/sh
        echo 'starting ios simulator debug build...'
        cd ios
        pod install
        xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Debug
        xcodebuild archive -quiet -archivePath ./build/debugsimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Debug
        ```

        [Xcode зборка через термінал](///Дополнительные-странницы///Xcode-сборка-через-термінал)

    2.  `simulator-release-build.sh`:

        ```
        #!/bin/sh
        echo 'starting ios simulator release build...'
        cd ios
        pod install
        xcodebuild build -quiet CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' -configuration Release
        xcodebuild archive -quiet -archivePath ./build/debugsimulatorapp -workspace expotestapp.xcworkspace -scheme expotestapp -destination 'generic/platform=iOS Simulator' CODE_SIGN_STYLE=Automatic DEVELOPMENT_TEAM=Y6TZPN9V5V -configuration Release
        ```
        [Xcode зборка через термінал](///Дополнительные-странницы///Xcode-сборка-через-термінал)

5.  Перезапустити VSCode

6.  Відкрити командну палітру. Для macos поєднання клавіш `⌘ + P, F1`, для Windows `CTRL + Shift + P`. У пошуку написати `Run Task`, вибрати відповідний елемент зі списку. Вибрати задачу

    [<img src="///Images//IDE/vscode-run-task.png" width="500">](///Images//IDE/vscode-run-task.png)

    [<img src="///Images//IDE/vscode-tasks.png" width="500">](///Images//IDE/vscode-tasks.png)