- [GitLab Pages](#gitlab-pages)
  - [Сборка проекта на GitLab](#сборка-проекта-на-gitlab)
- [Сloudflare](#сloudflare)
  - [Подключение Сloudflare к GitLab](#подключение-сloudflare-к-gitlab)
- [GitLab Runner](#gitlab-runner)
  - [Установка, настройка и запуск GitLab Runner](#установка-настройка-и-запуск-gitlab-runner)
    - [MacOS](#macos)
    - [Windows](#windows)
    - [Docker](#docker)
- [Дополнительная информация по GitLab Runner](#дополнительная-информация-по-gitlab-runner)
  - [Настройка и управление раннерами CI/CD в GitLab](#настройка-и-управление-раннерами-cicd-в-gitlab)

# GitLab Pages

## Сборка проекта на GitLab:

1. Создать файл .gitlab-ci.yml в корневой папке проекта.
2. Добавить в файл .gitlab-ci.yml конфигурацию конвейера CI/CD, включая этапы и задачи, которые необходимо выполнить. Пример правильной настройки этого файла:

   ```plaintext
   image: node:latest

   pages:
     stage: deploy
     cache:
       paths:
         - node_modules/
     script:
       - npm install
       - ./node_modules/.bin/nx build react-web
       - rm -rf public
       - mv dist/apps/react-web/.next public
     artifacts:
       paths:
         - public
     only:
       - master
   ```

   Значение команд:

   1. `image: node:latest:` Это указание означает, то GitLab Runner должен использовать Docker-образ с последней версией Node.js для выполнения задач в CI/CD.
   2. `pages:` Этот блок относится к развертыванию страниц на GitLab Pages.
   3. `stage: deploy:` Эта задача развертывания будет выполняться на этапе "deploy" (развертывание). Также можно использовать `stage: build:` - этап предназначен для [сборки](///III.-Дополнительные-пункты//Глоссарий#build) приложения или проекта и `stage: test:` - этап предназначен для выполнения автоматического тестирования проекта.
   4. `cache:` Здесь указаны пути к файлам и директориям, которые будут кэшироваться между запусками задачи для ускорения процесса. В данном случае, кэшируется директория node_modules/.
   5. `script:` Это список команд, которые будут выполнены в рамках задачи:
      1. `npm install:` Установка зависимостей проекта Node.js.
      2. `./node_modules/.bin/nx build react-web:` [Сборка](///III.-Дополнительные-пункты//Глоссарий#build) проекта с использованием инструмента Nx.
      3. `rm -rf public:` Удаление текущей директории public, если она существует.
      4. `mv dist/apps/react-web/.next public:` Перемещение собранных файлов из dist/apps/react-web/.next в созданую директорию public.
   6. `artifacts:` Здесь указаны пути к файлам или директориям, которые должны быть сохранены как артефакты после завершения задачи. В данном случае, сохраняется директория `public`, которая содержит собранные файлы. Чтобы приложение развернулось, обязательно нужно указать директорию с названием `public`. Также, если необходимо, можно указать пути к артефактам тестирования - `test` или статическим ресурсам - `static`.
   7. `only:` Этот параметр определяет условия, при которых задача будет выполнена. В данном случае, задача будет выполнена только для ветки с именем `master`. Также вместо названия ветки можно добавить значение `push`, тогда задача будет выполнена при каждом `push` в репозиторий. Или можно использовать значение `merge_requests`, тогда задача будет выполнена только при создании, обновлении или закрытии запросов на слияние.

3. Зафиксировать и отправить файл `.gitlab-ci.yml`.

# Сloudflare

## Подключение Сloudflare к GitLab:

1. Создать аккаунт на Сloudflare: https://dash.cloudflare.com/sign-up
2. В Сloudflare на боковой панели перейти в раздел Workers & Pages и выбрать Pages.
3. Нажать на кнопку Connect to git и выбрать раздел GitLab.
4. Ввести учетные данные от аккаунта GitLab.
5. Выбрать репозиторий, к которому нужно подключиться, и нажать Begin setup.
6. Заполнить поля:

   1. Project name - ввести название проекта, например, ReactWeb.
   2. Production branch - указать название ветки:
      1. nx - указать ветку master
      2. unity -
   3. Framework preset - выбрать фреймворк, который используется в проекте:
      1. nx - указать None
      2. unity -
   4. Build command - ввести команду для [сборки](///III.-Дополнительные-пункты//Глоссарий#build) проекта:

      1. nx -

      ```
      ./node_modules/.bin/nx build react-web
      ```

      2. unity -

   5. Build output directory - указать путь к директории, в которой будут храниться файлы [сборки](///III.-Дополнительные-пункты//Глоссарий#build) проекта:

      1. nx -

      ```
      dist/apps/react-web/.next
      ```

      2. unity -

   6. Root directory - корневой каталог проекта, в котором Cloudflare запускает команду [сборки](///III.-Дополнительные-пункты//Глоссарий#build):
      1. nx - заполнять не нужно
      2. unity -
   7. Environment variables - переменные, которые будут использоваться во время [сборки](///III.-Дополнительные-пункты//Глоссарий#build):
      1. nx - ничего добавлять не нужно
      2. unity -

[<img src="///Images/ci_cd/cloudflare.jpeg" width="500" height="auto">](/Images/ci_cd/cloudflare.jpeg)

7. Нажать Save and deploy. После этого проект развернется на Сloudflare.

# GitLab Runner

На **віддаленому** macOS налаштовувати GitLab Runner **не потрібно**. Можна налаштувати GitLab Runner на локальному комп'ютері.

#### Если требуется настройка раннера на удалённом компьютере:

1. Убедится что на компьютере установлен только **один** раннер.
2. Настраивать раннер чтоб запускался атоматически при запуске компьютера.


Чтобы раннер запускался автоматически при запуске компьютера нужно перейти в  System Settings->General

[<img src="///Images/ci_cd/settings_login_items.png" width="500" height="auto">](/Images/ci_cd/settings_login_items.png)

Скопировать путь к git-lab раннеру 

[<img src="///Images/ci_cd/check_path_runner.png" width="500" height="auto">](/Images/ci_cd/check_path_runner.png)

[<img src="///Images/ci_cd/check_path_runner.png" width="500" height="auto">](/Images/ci_cd/check_path_runner.png)

добавить git-lab раннер к числу приложений которые буду открываться при запуске компьютера. Нужно нажать на `+` 

[<img src="///Images/ci_cd/add_runner_to_OpenInSign_aplications.png" width="500" height="auto">](/Images/ci_cd/add_runner_to_OpenInSign_aplications.png)

В Finder-е нажать комбинацию клавиш `shift + g + command` 

[<img src="///Images/ci_cd/addPathToFinder.png" width="500" height="auto">](/Images/ci_cd/addPathToFinder.png)

Скопировать путь , нажать на приложение git-lab runner  и `open`

[<img src="///Images/ci_cd/add_git_lab_runner_to_open_in_login.png" width="500" height="auto">](/Images/ci_cd/add_git_lab_runner_to_open_in_login.png)


## Именование раннера 
#### На удалённом  корпаративном компьютере
 
Раненнеры на удалённом копьютере настраиваются через права администратора Тим лидом. 
Имя раннера на удалённом копьютере должны быть в таком формате: `Операционна система_Модель процессора_Имя пользователя Mac OS`
| не правильный пример    | правильный пример |
| -------- | ------- |
|  `teal`  | `MacOS_intell_teal`    |
| `user2_M1_macOS` | `MacOS_M1_user2`    |
| `MacOS-M2-maroon`    | `MacOS_M2_maroon`    |
| `Intell_MZ_runner`    | `Win10_intell_MZ`    |


#### На локальном компьютере пользователя 

При создании локального ранного, название раннера должно быть в таком формате: `Операционна система_Модель процессора_LEO` - где  LEO - Инициалы разработчика

| не правильный пример    | правильный пример |
| -------- | ------- |
|  `Ubuntu24`  | `Ubuntu24.4_intell_BMA`    |
| `KBY_runner` | `Win11_intell_KBY`    |




## Установка, настройка и запуск GitLab Runner

### MacOS:

1. Установить runner: https://docs.gitlab.com/runner/install/osx.html. Выбрать бинарный файл для текущей системы: на базе Intel или Apple Silicon.
2. [Создать и зарегистрировать runner проекта](#создание-и-регистрация-gitlab-runner)
3. [Настроить файл config.toml](#настройка-файла-configtoml)

### Windows:

1. Установить runner: https://docs.gitlab.com/runner/install/windows.html. Выбрать бинарный файл для текущей системы: для 64-битной или 32-битной версии.
2. [Создать и зарегистрировать runner проекта](#создание-и-регистрация-gitlab-runner)
3. [Настроить файл config.toml](#настройка-файла-configtoml)

### Docker:

1. Установить docker : https://docs.docker.com/get-docker/. Выбрать платформу.
2. Создать и зарегистрировать runner проекта через Docker: https://docs.gitlab.com/runner/install/docker.html. Поле tags для runner необязательно, нужно выбрать 'Run untagged jobs'. Один runner можно добавить сразу к нескольким проектам.
3. Запустить runner командой :
   ```
   docker start gitlab-runner
   ```

# Дополнительная информация по GitLab Runner:

## Типи GitLab Runners

GitLab Runners бывают двух типов

1. [Shared Runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners)
2. [Project Runners](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#project-runners)

### Shared Runners (Глобальный раннер) 

- Доступен для каждого проекта на GitLab
- Только администратор может создавать такой раннер 
- Администратор может создать [максимальное количество минут подключения для кождой групы](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-compute-quota-for-a-specific-namespace)

### Project Runners 

- Для создание Project раннера необходимо иметь роль Maintainer на проекте  
- Раннер можно использовать в других проектах, если он не закрыт в других проектах , для этого должна отсутствовать галочка возле `When a runner is locked, it cannot be assigned to other projects`: 

[<img src="///Images/ci_cd/unlocked-runner.png" width="500" height="auto">](/Images/ci_cd/unlocked-runner.png)

- Для того, чтобы использовать созданный раннер в любом другом  проекте, нужно чтоб в проекте, к котором будет подключаться созданный `Project Runner`, у пользователя была роль maintainer  
 
 

## Подключение раннера к проекту

Для выполнения этой инструкции права доступа к репозиторию должны быть: [maintainer](///III.-Дополнительные-пункты///Ролі-доступу-в-GitLab)

Чтобы добавить  runner к проекту  нужно чтобы в спике раннеров  были активные  для конкретной операционной системы ранеры

[<img src="///Images/Runner/active_runners.png" width="500">](///Images/Runner/active_runners.png)


И далее нажать 'Enable for this project'

[<img src="///Images/Runner/enable_runner.png" width="500">](///Images/Runner/enable_runner.png)

Если в сборке нету тегов, то нужно убедиться, что в раннере стоит галочка `Indicates whether this runner can pick jobs without tags' , если нету галочки, то  нужно поставить галочку.

Чтобы поставить галочку нужно перейти в edit раннера 

[<img src="///Images/Runner/edit_runner.png" width="500">](///Images/Runner/edit_runner.png)

Поставить галочку, если она отсутствует и сохранить настройки 

[<img src="///Images/Runner/save_changes_runner.png" width="500">](///Images/Runner/save_changes_runner.png) 

## Создание и регистрация GitLab Runner

Если отсутствуют подходящие глобальные раннеры, нужно создавать локальный раннер

Если пользователь имеет роль доступа в GitLab maintainer то будет создан Project runner, если administator  - Shared runner   

1. [Создать и зарегистрировать runner проекта](https://docs.gitlab.com/ee/tutorials/create_register_first_runner/index.html#create-and-register-a-project-runner)

   [<img src="///Images/Runner/CreateRunnerGitlab.png" width="500">](///Images/Runner/CreateRunnerGitlab.png)

   
В поле tags необходимо вписать следующие теги:

1. Версия ОС: `MacOS`, `Windows`, `Linux`.
2. Определите теги для различных версий Unity, IOS, JDK и NDK. Например:

Unity: `unity2019`, `unity2020`, `unity2021`, и т.д.

JDK: `jdk8`, `jdk11`, `jdk15`, и т.д.

NDK: `ndk-r21`, `ndk-r22`, `ndk-r23`, и т.д.

IOS: `ios14`, `ios16`, `ios17`.

Необязательно добавлять поле tags для runner. Теги указывают, какие задания может выполнять runner и являются необязательными. Для этого нужно выбрать 'Run untagged jobs'. Также можно добавить один runner к нескольким проектам.


При регистрации раннера может вылететь такая ошибка:

[<img src="///Images/ci_cd/http_eror_runner.png" width="300" height="auto">](/Images/ci_cd/http_eror_runner.png)

Для того, чтобы  исправить эту ошибку, нужно в команде терминала изменить url, к котрому подключается раннер с http на https 

2. После регистрации, в файле config.toml появятся необходимые поля, как показано в примере ниже.

### Автозапуск GitLab runner на MacOS

Подробнее об автозапуске раннера вы можете прочитать [здесь](https://docs.gitlab.com/runner/install/osx.html#limitations-on-macos)

Вы можете убедиться, что GitLab Runner создал файл конфигурации службы после выполнения install команды, проверив `~/Library/LaunchAgents/gitlab-runner.plist` файл.

Также необходимо убедится что в настройках активированы параметры для автозапуска. Для этого перейдите в Настройки -> Основные -> Объекты входа и убедитесь в том что эти 3 значения включены:

[<img src="///Images/ci_cd/runner_settings.png" width="300" height="auto">](/Images/ci_cd/runner_settings.png)

Если все настроено правильно то после перезагрузки системы раннер будет автоматически включатся.


## Настройка файла config.toml

Настроить файл config.toml: Найти файл config.toml в проекте или где установлен runner и добавить: `shell = "sh"`. Пример:

```plaintext
[[runners]]
  name = "runner1"
  url = "https://gitlab.com"
  id = 28166112
  token = "Your_token"
  token_obtained_at = 2023-10-04T08:26:34Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "sh"
```

    Значение команд:

1.  `name:` Это имя GitLab Runner. Необязательный параметр, GitLab Runner может имет любое имя Используется для идентификации runner в интерфейсе GitLab.
2.  `url:` Это URL-адрес GitLab-сервера.
3.  `id:` Это уникальный идентификатор (ID) GitLab Runner, что генерируется автоматически при регистрации runner на сервере GitLab.
4.  `token:` Это токен авторизации, который используется для безопасной связи между GitLab Runner и GitLab-сервером. Токен также генерируется при регистрации runner и должен быть секретным.
5.  `token_obtained_at:` Это дата и время получения токена. Используется для отслеживания срока действия токена.
6.  `token_expires_at:` Это дата и время истечения действия токена. В данном случае, установлено значение по умолчанию, указывающее на то, что токен не истекает. Однако в реальных настройках GitLab Runner, токен может иметь ограниченное время действия.
7.  `executor:` Это тип исполнителя (executor), который будет использоваться для выполнения задач. Здесь указан "shell", что означает, что задачи будут выполняться в оболочке командной строки. Можно использовать и другие исполнители, такие как: "docker-autoscaler", "instance", "custom", "docker-windows", "ssh", "virtualbox", "docker+machine", "kubernetes", "docker", "parallels".
8.  `shell:` Это оболочка командной строки, которая будет использоваться в качестве исполнителя (shell executor). В данном случае, указана "sh". Можно использовать и другие оболочки, такие как: "powershell", "bash", "zsh", "cmd".
На Windows устройстве, при создании `config.toml` при помощи Windows PowerShell, дефолтное значение `shell` "pwsh", нужно сменить на "powershell", иначе задание (job) Gitlab Runner не выполнится успешно.
Важно настроить автоматический запуск GitLab Runner после перезапуска Mac устройства:
9.  Установить GitLab Runner как сервис и запустить его:
    ```
    gitlab-runner install
    gitlab-runner start
    ```
10. Перезагрузить систему.

**Примечание:** 1. Если `gitlab-runner install` неудачен (fail), нужно проверить наличие доступа администратора текущей сессии или перезапустить PC. 2. Если инициализация `gitlab-runner run` занимает много времени и результат неудачен (fail), тогда использовать
```
gitlab-runner start
gitlab-runner restart
```

## Оновлення GitLab Runner

Спершу необхідно впевнитися, що GitLab Runner потребує оновлення. Для цього:

1. Зайти на [сторінку GitLab Runner релізів](https://gitlab.com/gitlab-org/gitlab-runner/-/releases), знайти останню найвищу версію

2. Перевірити версію GitLab Runner що стоїть на комп'ютері командою:

```
gitlab-runner -v
```

Потім оновити GitLab Runner згідно інструкціям:

- [Windows](https://docs.gitlab.com/runner/install/windows.html#update)
- [macOS](https://docs.gitlab.com/runner/install/osx.html#upgrade-gitlab-runner)
- [Linux](https://docs.gitlab.com/runner/install/linux-repository.html#updating-gitlab-runner)

## Перевірка GitLab Runner-ів на комп'ютері

### Верифікація раннерів

Для перевірки раннерів, які можуть підключитися до GitLab:

```
gitlab-runner verify
```

У випадку якшо `config.toml` знаходиться не в дефолтній директорії вказати шлях до нього:

```
gitlab-runner verify -c ../config.toml
```

### Список раннерів

Для виведення списку раннерів які є в `config.toml` файлі:

```
gitlab-runner list
```

У випадку якшо `config.toml` знаходиться не в дефолтній директорії вказати шлях до нього:

```
gitlab-runner list -c ../config.toml
```

## Вероятные ошибки при установке и настройке gitlab-runner

### Access denied

Код ошибки: `.\\gitlab-runner-windows-amd64.exe install fail` 

Если терминал запущен от имени администратора и при установке `gitlab-runner` происходит такая ошибка нужно перезапустить ПК и попробовать снова.

### Stuck

Ошибка: `.\\gitlab-runner-windows-amd64.exe run initialization stuck`

Если происходит такая ошибка нужно провести запустить по очереди команды `start` `restart`

### Нет подходящего Shell

Ошибка `job fail через config.toml shell = "pwsh"`. Проверить [переменные среды](Дополнительные-странницы///Як-додати-глобальну-змінну-оточення). Проверить другие типы shell оисанные в [инструкции по настройке `config.toml`](II.-Техническая-(справочная)-инструкции/CI-и-CD#настройка-файла-configtoml).

## Настройка и управление раннерами CI/CD в GitLab

 ### Настройка кэша
  Для настройки кэша в файле `.gitlab-ci.yml` добавьте раздел `cache`:

    ```yaml
    cache:
      paths:
        - .cache/
    ```

  В этом примере GitLab CI/CD будет сохранять содержимое каталога `.cache/` между сборками.

 ### Настройка артефактов
  Для настройки артефактов в файле `.gitlab-ci.yml` добавьте раздел `artifacts`:

    ```yaml
    job:
      artifacts:
        paths:
          - build/
    ```

  В этом примере GitLab CI/CD будет сохранять содержимое каталога `build/` после сборки.

 ### Настройка времени хранения
  Для настройки времени хранения в разделе `artifacts` укажите `expire_in`:
    

    ```yaml
    job:
      artifacts:
        paths:
          - build/
        expire_in: 1 week
    ```

  В этом примере GitLab CI/CD будет удалять артефакты через неделю после создания.

 ### Настройка запуска раннера
  Для настройки запуска раннера в файле `.gitlab-ci.yml` используйте теги:

    ```yaml
    job:
      tags:
        - my-runner
    ```

  В этом примере GitLab CI/CD будет использовать раннер с тегом `my-runner` для выполнения этой задачи.

 ### Редактирование тегов и описания раннера

  - Войдите в GitLab и перейдите на страницу вашего проекта.
  - Перейдите в `Settings` -> `CI/CD`.
  - Прокрутите вниз до раздела `Runners`.
  - Найдите раннер, который вы хотите отредактировать, и нажмите на кнопку с карандашом (Редактировать).
  - В открывшемся окне вы можете изменить `Description` (Описание) и `Tags` (Теги) раннера.
  - После внесения изменений нажмите `Save changes` (Сохранить изменения).
  - Важно: Теги, которые вы добавляете к раннеру, должны соответствовать тегам, указанным в файле 
    `.gitlab-ci.yml`. Если они не совпадают, раннер не сможет выполнить задачи с неподходящими тегами.
  - Повторите эти шаги для каждого раннера, который вы хотите отредактировать.
