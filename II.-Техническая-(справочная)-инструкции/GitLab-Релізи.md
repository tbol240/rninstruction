# Зміст

1. [GitLab релізи з Package Registry при створенні тегу](#gitlab-релізи-з-package-registry-при-створенні-тегу)
   1. [Для React Native](#для-react-native)
   2. [Для Unity](#для-unity)
2. [GitLab релізи з артефактами при створенні тегу](#gitlab-релізи-з-артефактами-при-створенні-тегу)
   1. [Для React Native](#для-react-native-1)
3. [GitLab релізи з Package Registry при merge на основну гілку](#gitlab-релізи-з-package-registry-при-merge-на-основну-гілку)
   1. [Для React Native](#для-react-native-2)

Для створення [GitLab релізів](https://docs.gitlab.com/ee/user/project/releases/) необхідно мати роль доступу [maintainer](///III.-Дополнительные-пункты///Ролі-доступу-в-GitLab) або [owner](///III.-Дополнительные-пункты///Ролі-доступу-в-GitLab) в репозиторії

# [GitLab релізи](https://docs.gitlab.com/ee/user/project/releases/) з Package Registry при створенні тегу

## Для React Native

Для налаштування [GitLab релізів](https://docs.gitlab.com/ee/user/project/releases/):

1. [Встановити release-cli](https://docs.gitlab.com/ee/user/project/releases/release_cli.html)

2. [Cтворити релізний keystore файл](https://reactnative.dev/docs/signed-apk-android#generating-an-upload-key) якщо він ще не створений для підпису релізної android сборки. Під час створення keystore файлу, знадобиться згенерувати пароль, вказати аліас до нього. Зберегти пароль, аліас і створений keystore файл. Створений keystore файл вивантажити на GitLab `Setting > CI/CD > Secure Files`

3. Створити [Deploy Token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) для проекту:

   1. Зайти на GitLab в `Settings > Repository > Deploy Tokens`. Натиснути `Add token`

   2. Дати унікальне ім'я серед інших деплой токенів, в `Scopes` обрати всі три пункти

      [<img src="///Images/gitlab-releases/create-deploy-token.png" width="500">](///Images/gitlab-releases/create-deploy-token.png)

   3. Зберегти `username` і `password` згенерованого токену

      [<img src="///Images/gitlab-releases/save-deploy-token.png" width="500">](///Images/gitlab-releases/save-deploy-token.png)

4. Додати наступні змінні в `GitLab Settings > Variables`:

   1. `ANDROID_KEYSTORE_ALIAS`. Аліас до релізного keystore файлу. Приклад значення: `release-test-app`
   2. `ANDROID_KEYSTORE_FILE`. Шлях до релізного keystore файлу. Приклад значення: `./release-test-app.keystore`
   3. `ANDROID_KEYSTORE_PASSWORD`. Пароль до релізного keystore файлу.
   4. `DEPLOY_TOKEN_PASS`. Пароль деплой токену.
   5. `DEPLOY_TOKEN_USERNAME`. Юзернейм деплой токену.
   6. `PACKAGE_VERSION`. Версія релізу. Приклад значення: `1.0.0`

5. [Додати/редагувати `.gitlab-ci.yml` файл](///Дополнительные-странницы///Релізи-RN-CI-файл)

6. Створити новий [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8). Створити [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8) можливо через термінал або GitLab. Для створення git tag через GitLab:

   1. Зайти в `Code > Tags`. Натиснути `New tag`

      [<img src="///Images/gitlab-releases/where-create-tag.png" width="500">](///Images/gitlab-releases/where-create-tag.png)

   2. Дати ім'я тегу. Наприклад `1.0.0`, вибрати гілку з якої буде відбуватися реліз, натиснути `Create tag`. На цьому етапі в `Pipelines` має з'явитися новий пункт

      [<img src="///Images/gitlab-releases/new-release-pipeline.png" width="500">](///Images/gitlab-releases/new-release-pipeline.png)

7. Запустити необхідні білди, після їх успішного завершення запустити `Release` Job

   [<img src="///Images/gitlab-releases/jobs-in-release-pipeline.png" width="500">](///Images/gitlab-releases/jobs-in-release-pipeline.png)

Для кожного наступного релізу:

1. Інкрементувати версію застосунку в `app.json`. Інкрементувати наступні поля: `version`, `ios.buildNumber`, `android.versionCode`.

2. Інкрементувати змінну `PACKAGE_VERSION` на GitLab

## Для Unity

1. [Встановити release-cli](https://docs.gitlab.com/ee/user/project/releases/release_cli.html)

2. Створити [Deploy Token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) для проекту:

   1. Зайти на GitLab в `Settings > Repository > Deploy Tokens`. Натиснути `Add token`

   2. Дати унікальне ім'я серед інших деплой токенів, в `Scopes` обрати всі три пункти

      [<img src="///Images/gitlab-releases/create-deploy-token.png" width="500">](///Images/gitlab-releases/create-deploy-token.png)

   3. Зберегти `username` і `password` згенерованого токену

      [<img src="///Images/gitlab-releases/save-deploy-token.png" width="500">](///Images/gitlab-releases/save-deploy-token.png)

3. Додати наступні змінні в `GitLab Settings > Variables`:

   1. `Build_iOS`. Версія iOS бандлу. Приклад значення: `1.0.0`
   2. `Bundle_Version_Code`. Версія Android бандлу. Приклад значення: `1`
   3. `POD`. [Значення](///Дополнительные-странницы//POD-файл)
   4. `Project_ID`. Ідентифікатор Unity застосунку. Приклад значення `unity-test-3`
   5. `Target_SDK`. Мінімальну версія iOS таргету. Значення `12.0`
   6. `UNITY_LOCATION_MACOS`. Путь к юнити едитору для macOS, нужно быть уверенным, что едитор установлен и находится на указаном пути, скорее всего для macOS не нужно будет менять путь, установка на них происходит обычно в одно и тоже место. Пример значения: `"2021.3.26f1 , installed at /Applications/Unity/Hub/Editor/2021.3.26f1/Unity.app/Contents/MacOS/Unity"`
   7. `UNITY_LOCATION_WINDOWS`. Путь к юнити едитору для windows, нужно быть уверенным, что едитор установлен и находится на указаном пути, для windows скорее всего нужно будет менять путь для разных устройств. Пример значения: `"2021.3.26f1 , installed at G:\\Unity\\2021.3.26f1\\Editor\\Unity.exe"`
   8. `UNITY_VERSION`. Номер версии, на которой происходит сборка, менять в зависимости от используемой версии Юнити. Приклад значення: `2021.3.26f1`
   9. `DEPLOY_TOKEN_PASS`. Пароль деплой токену.
   10. `DEPLOY_TOKEN_USERNAME`. Юзернейм деплой токену.
   11. `PACKAGE_VERSION`. Версія релізу. Приклад значення: `1.0.0`

4. [Додати/редагувати `.gitlab-ci.yml` файл](///Дополнительные-странницы///Релізи-Unity-CI-файл)

5. Створити новий [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8). Створити [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8) можливо через термінал або GitLab. Для створення git tag через GitLab:

   1. Зайти в `Code > Tags`. Натиснути `New tag`

      [<img src="///Images/gitlab-releases/where-create-tag.png" width="500">](///Images/gitlab-releases/where-create-tag.png)

   2. Дати ім'я тегу. Наприклад `1.0.0`, вибрати гілку з якої буде відбуватися реліз, натиснути `Create tag`. На цьому етапі в `Pipelines` має з'явитися новий пункт

      [<img src="///Images/gitlab-releases/new-release-pipeline.png" width="500">](///Images/gitlab-releases/new-release-pipeline.png)

6. Запустити необхідні білди, після їх успішного завершення запустити `Release` Job

   [<img src="///Images/gitlab-releases/unity-jobs-in-release-pipeline.png" width="500">](///Images/gitlab-releases/unity-jobs-in-release-pipeline.png)

# GitLab релізи з артефактами при створенні тегу

## Для React Native

Автоматично добавляти всі посилання на артефакти в релізі в Pipeline можливо тільки використовуючи macOS і роблячи всі зборки в одному Job

Для створення GitLab релізів з посиланнями на артефакти необіхдно:

1. Створити/редагувати [.gitlab-ci.yml файл](///Дополнительные-странницы///Релізи-через-артефакти-RN)

2. Створити новий [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8). Створити [git tag](https://git-scm.com/book/ru/v2/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%A0%D0%B0%D0%B1%D0%BE%D1%82%D0%B0-%D1%81-%D1%82%D0%B5%D0%B3%D0%B0%D0%BC%D0%B8) можливо через термінал або GitLab. Для створення git tag через GitLab:

   1. Зайти в `Code > Tags`. Натиснути `New tag`

      [<img src="///Images/gitlab-releases/where-create-tag.png" width="500">](///Images/gitlab-releases/where-create-tag.png)

   1. Дати ім'я тегу. Наприклад `1.0.0`, вибрати гілку з якої буде відбуватися реліз, натиснути `Create tag`. На цьому етапі в `Pipelines` має з'явитися новий пункт

      [<img src="///Images/gitlab-releases/new-release-pipeline.png" width="500">](///Images/gitlab-releases/new-release-pipeline.png)

3. Запустити єдинний Job

# GitLab релізи з Package Registry при merge на основну гілку

## Для React Native

1. Встановити [release-cli](https://docs.gitlab.com/ee/user/project/releases/release_cli.html) якщо ще не встановлений

2. [Cтворити релізний keystore файл](https://reactnative.dev/docs/signed-apk-android#generating-an-upload-key) якщо він ще не створений для підпису релізної android сборки. Під час створення keystore файлу, знадобиться згенерувати пароль, вказати аліас до нього. Зберегти пароль, аліас і створений keystore файл. Створений keystore файл вивантажити на GitLab `Setting > CI/CD > Secure Files`

3. Створити [Deploy Token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) для проекту:

   1. Зайти на GitLab в `Settings > Repository > Deploy Tokens`. Натиснути `Add token`

   2. Дати унікальне ім'я серед інших деплой токенів, в `Scopes` обрати всі три пункти

      [<img src="///Images/gitlab-releases/create-deploy-token.png" width="500">](///Images/gitlab-releases/create-deploy-token.png)

   3. Зберегти `username` і `password` згенерованого токену

      [<img src="///Images/gitlab-releases/save-deploy-token.png" width="500">](///Images/gitlab-releases/save-deploy-token.png)

4. Створити [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)

   1. Зайти на GitLab в `Settings > Access Tokens`

   2. Натиснути `Add new token`

   3. Дати токену будь-яке ім'я, роль вибрати `maintainer`, в секції `scopes` поставити галочки напроти всіх пунтктів

5. Додати на GitLab в `Settings > CI/CD > Variables` наступні змінні:

   1. `ACCESS_TOKEN_PASS`. Пароль створеного `Access Token`
   1. `ANDROID_KEYSTORE_ALIAS`. Аліас до релізного keystore файлу. Приклад значення: `release-test-app`
   1. `ANDROID_KEYSTORE_FILE`. Шлях до релізного keystore файлу. Приклад значення: `./release-test-app.keystore`
   1. `ANDROID_KEYSTORE_PASSWORD`. Пароль до релізного keystore файлу.
   1. `DEPLOY_TOKEN_PASS`. Пароль деплой токену.
   1. `DEPLOY_TOKEN_USERNAME`. Юзернейм деплой токену.
   1. `PACKAGE_VERSION`. Версія релізу. Приклад значення: `1.0.0`

6. Додати/редагувати [.gitlab-ci.yml файл](///Дополнительные-странницы///gitlab-реліз-при-merge-CI)

7. `PACKAGE_VERSION` змінну на GitLab інкрементувати при кожному merge на основну гілку, а також інкрементувати версію застосунку в `app.json`. Інкрементувати наступні поля: `version`, `ios.buildNumber`, `android.versionCode`.

8. Створити і підтвердити [merge request](///III.-Дополнительные-пункты//Глоссарий#merge)
