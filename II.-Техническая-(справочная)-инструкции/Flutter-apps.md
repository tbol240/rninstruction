# Start application

## For Android
* Choose device.
* Go to /pubspec.yaml.
* Click `Pub get`.
* Click `Run` (Shift + F10).

#### Or

* Choose device.
* Open terminal in root directory.
```
flutter pub get
flutter run
```

## For IOS
* Open terminal in root directory.
```
flutter packages get
cd ios
pod install
```
* If you have problems with pods in xcode, in os directory type
```
pod deintegrate 
pod install
```
* Open ios directory in XCode.
* Choose device.
* Click Run button. 
* If the problem with pods persists, you may need to remove the Podfile.lock and repeat the steps.

#### Or
* Check if App.framework file exists in build -> ios -> Debug-iphonesimulator -> App.framework
* If it doesn't exist, follow the next steps.
* Make sure the FLUTTER_ROOT environment variable is set and points to the Flutter SDK root folder.
* Open your project in Xcode.
* In the "Build Settings" of your project in Xcode, go to "User-Defined"
* Check that the paths for FLUTTER_ROOT, FLUTTER_TARGET and PACKAGE_CONFIG are correct.
* Make sure you enter the correct paths.
* If the issue persists, try reinstalling the Flutter SDK and restarting Xcode.

## If you encounter the error 'Flutter/Flutter.h' file not found, or Flutter module not found when launching a project on iOS
* Backup your ios folder
* Delete ios folder
* Open a terminal in the folder where the project is located and type
``` 
flutter create [name of the project] 
```
* After entering this command the ios folder should be recreated.
* copy the Runner folder from the previously saved ios folder to the ios folder in the project.
* Open Runner.xcworkspace in XCode.
* Move Configuration.storekit and `Runner\GoogleService-Info.plist` files in XCode to the Runner tab part of the window on the left.
* Close XCode.
* Enter commands:
```
pod deintegrate
pod install 
```

## For MacOs versions
* Open terminal in root directory and type:
```
flutter packages get
cd macos
pod install
```
* If you have problems with pods in xcode, in os directory type:
```
pod deintegrate
pod install
```
* Open your IDE (Vs Code, XCode etc.)
* From devices list choose macOs
* Click Run button.

## If a macOS project is being built but the window is not displayed
* Create a separate test project.
* From the file `macos/Runner/MainFlutterWindow.swift` copy the code into the anoligical file of our project (or replace our file with this one).

## If errors occur during the debug/release of the macOS version due to an unsuitable Deployment Target
 Open the Podfile, and change the value in quotes platform :osx to a suitable Target, for example 10.15.
 For this example, the result should be: platform :osx "10.15"
 The same steps may help with this problem on the ios version.


# Deployment instructions

* Before deployment check app version and build version in pubspec.yaml
* Change versions to new

## For Android
* In terminal type 
```
flutter build appbundle
```
* If you need to test build on your device, type
``` 
flutter build apk
```
* install and test it on your device
* Go to Google Play Console website
* Enter, using developer account
* Choose application
* In left menu, select "Releases"
* Click "Create new release"
* Rename appbundle file to next build number, for example: "84.aab, 85.aab, 86.aab, etc."
* Upload appbundle file
* Fill information about version changes on different languages.
* Click Save
* Click Send to Check
* Check can take some days or hours

## For Ios and MacOs
* Open ios/macos folder of project in Xcode
* Check version and build number, change to new
* Open ios/macos folder in Xcode, or open Runner.xcworkspace in ios/macos directory
* in Product -> Destination select ios/macos device
* select Product -> Archive
* click Validate and do appearing instructions
* after validating click Distribute
* do the same instructions, select automatic signing, select appstore connect
* if program asks for account, use developers account
* After uploading file, go to AppStore connect
* Enter, using developer account
* Select application
* On the left side above current version of macos/ios, click "+" to add new release
* Fill information about changes in version in different languages
* Add bundle to project
* Before submitting test your app, see  info below
* If you can't see new bundle, you need to wait some time, until your uploaded bundle will appear
* After filling information correctly and selecting bundle, click "Save" and "Send for Check"

## Test release version before uploading on Ios
* You need to use mac with M1 chip
* In Destinations select "MyMac(Designed for iPad)"
* In Product -> Schemes -> Edit scheme, for "Run" select "Release"
* Build
* If will appear errors, like "Firebase.h/FirebaseCore.h/etc not found"
* Go to user directory, then `Library/Developer/Xcode/DerivedData` and delete all project data
* if you can't find Library folder, press "Cmd + Shift + >", it's hidden by default
* Then, go to ios folder in project and delete `Podfile.lock`, Pods folder`,  and .xcworkspace file
* In ios folder open terminal and type
``` 
Pod install
```
* Open new .xcworkspace file and build
* If will appear error, with non selected signature in pods
* Go to Pods in Xcode, select pods, which was mentiones in errors, and select developers team in them

## if you encounter the error 'Build service could not create build operation: unknown error while handling message: 
## MsgHandling Error (message: "unable to initiate PIF transfer session (operation in progress?)")
* Closing and opening Xcode seems to remove this error.
or
* Go to xcode settings -> Locations -> DerivedData navigate to folder -> delete all -> build project

## How to test ios release subscription.
* First you need to add a test account to sandbox testers in Apple developers (You need a unique email to fill out the form)
* After adding, open the application (Ensure that the app is in release mode)
* Try to purchase a subscription
* You will see a form that requires an apple id(email) and password
* You need to fill these with the tester credentials that you have already added to sandbox testers
* So, apple will automatically recognize that you are in a sandbox environment, then your purchase will be successful

## How to test android release subscription before uploading
* Go to Google Play Console website
* Enter, using developer account
* Choose application
* In left menu, select "Testing"/"Close Testing"
* Tap "Create version"
* Give version a name that matches the bundle version name (176, 177, 178 etc.) and create
* Tap "Number of testers"
* In Email lists, select one of the lists or create a new one and add your email address that you use in Google Play
* Then click “Save” and “Create
* Then add the app bundle you want to test before release
* Fill the "Release name" and "Release Notes" if necessary
* Tap "Next", check the correctness of the filled information and click "Save"
* Then click "Go to review" or chose "Overview of the publication" in left menu
* Tap "Submit changes for review" 
* Аfter passing the verification (it may take several hours), download the application using the account indicated as a tester and verify the monetization
* See the status of the verification on the tab "Overview of releases" in the left menu
* After test suspend testing on the "Closed Testing" tab on the left menu

## Localization rules in project (if project uses Flutter Intl plugin from Localizely)
* To work with localization you must add Flutter Intl plugin from Localizely to your IDE 
* This plugin generate classes by .arb content files
* .arb files located in lib/l10n
* To add new string you can manually add it to all .arb files in key-value format or in the editor
* you can right click on String constant and press "Extract to ARB file" and select all languages
* This action create key-value string in all files, but you need to translate it by self
* To start plugin generator press Ctrl + S inside .arb file, it will re-generate all files
* To translate string you can use two functions:
  * S.current.key
  * or
  * S.of(context).key
* I advise to use first option, because it looks simpler and shorter and do not require BuildContext
