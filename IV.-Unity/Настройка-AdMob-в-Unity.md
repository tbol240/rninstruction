# Настройка рекламы AdMob

Для того чтобы добавить Admob нужно скачать с [сайта](https://github.com/googleads/googleads-mobile-unity/releases) GoogleMobileAds

Чтобы установить нужно сначала выбрать  версию и нажать на unitypackage. Это установит пакет для unity. 

[<img src="///Images//AdMob/googleMobileAds_download.png" width="500">](///Images//AdMob/googleMobileAds_download.png)

Далее нужно перетащить этот файл в Packages и нажать на import 

[<img src="///Images//AdMob/import_google_mobile_ads.png" width="500">](///Images//AdMob/import_google_mobile_ads.png)

## Подключение рекламы на сцене

Добавить из фреймворка префаб `AdMob`: 

[<img src="///Images//AdMob/AdMob_Prefab.png" width="500">](///Images//AdMob/AdMob_Prefab.png)

Далее настроить внутри префаба на сцене ID для рекламы. Тестовые ID можна найти в [документации Google](https://developers.google.com/admob/unity/test-ads?hl=ru#android).

Пример настройки Баннера: 

[<img src="///Images//AdMob/AdMob_Banner_Set.png" width="500">](///Images//AdMob/AdMob_Banner_Set.png)

Пример настройки Межстраничной рекламы: 

[<img src="///Images//AdMob/AdMob_Interstitial_Set.png" width="500">](///Images//AdMob/AdMob_Interstitial_Set.png)

Настроить тестовый профиль в настройках GoogleMobileAds: 

[<img src="///Images//AdMob/Asset_GoogleMobileAds_Set.png" width="500">](///Images//AdMob/Asset_GoogleMobileAds_Set.png)

## Возможные ошибки AdMob при сборке

### Ошибка при сборке из терминала. 

Вероятные пути решения:

Проверить что стоят нужные галочки в настройках проекта:

[<img src="///Images//AdMob/Unity_Publishing_Build.png" width="500">](///Images//AdMob/Unity_Publishing_Build.png)

Проверить зависимости в файле `mainTemplate.gradle` (Около версии не должны стоять квадратные скобочки):

[<img src="///Images//AdMob/AdMob_mainTemplate_gradle.png" width="500">](///Images//AdMob/AdMob_mainTemplate_gradle.png)

Проверить зависимости в файле `<ProjectFolder>\Assets\GoogleMobileAds\Editor\GoogleMobileAdsDependencies.xml` (Около версии не должны стоять квадратные скобочки):

[<img src="///Images//AdMob/Unity_GMA_Dependecies.png" width="500">](///Images//AdMob/Unity_GMA_Dependecies.png)

Проверить расположение файлов adMob в иерархии проекта. `GoogleMobileAds` `ExternalDependencyManager` `Plugins` должны находится в папке `<ProjectFolder>\Assets`:

[<img src="///Images//AdMob/Package_Folders.png" width="500">](///Images//AdMob/Package_Folders.png)

Если сборка уже проводилась до этих изменений, во время сборки из терминала может появится ошибка во время процесса сборки gradle:

`* What went wrong:`

`Execution failed for task ':launcher:dexBuilderRelease'.`

`> java.lang.UnsupportedOperationException: This feature requires ASM7.`

Решается удалением папки `<ProjectFolder>\Library\Bee\Android`. И перезапуском сборки.